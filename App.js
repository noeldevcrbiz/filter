import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput, TouchableOpacity, StatusBar, ImageBackground, KeyboardAvoidingView, AsyncStorage} from 'react-native';
import { Container, Toast,Root,Spinner, Button, Row, Col, Icon, Content, Item, Form, Header, Body, Left, Right, Title} from 'native-base';
import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import { Font, Apploading} from 'expo';

import Dashboard from './components/Dashboard/Dashboard';
import Login from './components/Login/Login';

import ViewKitDesign from './components/Dashboard/ChangePasswordScreen';


class SignInScreen extends React.Component {
  constructor() {
    super();
   /* this.state = {
      spinner: false
    };*/
    global.rootDom = 'https://filterandindustrial.com.au';
    global.tokenx = '';
    global.accessx = '';

    //account info
    global.firstLetterx = '';
    global.fNamex = '';
    global.lNamex = '';
    global.emailx = '';
    
    global.kitproductIDxx = '';
    global.kitIDx = '';
    global.navFrom = '';
    global.counterx = ''; // use for checking of product list
  }

  componentWillMount() {
    this.loadFonts();
    StatusBar.setHidden(true);
  }

  async loadFonts() {
    await Expo.Font.loadAsync({
      Roboto: require("./node_modules/native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("./node_modules/native-base/Fonts/Roboto_medium.ttf"),
    });
    this.setState({ isReady: true });
  }



  handleClick = () => {

    return fetch('https://filterandindustrial.com.au/apix/member/Login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        apikey: 1,
        username: this._email._lastNativeText,
        password: this._password._lastNativeText,
      }),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.code=="0"){
        global.tokenx = responseJson.data.auth.token;
        global.accessx = responseJson.data.auth.access;

        global.fNamex = responseJson.data.userinfo.first_name;
        global.lNamex = responseJson.data.userinfo.last_name;
        global.firstLetterx = responseJson.data.userinfo.first_capital;
        global.emailx = responseJson.data.userinfo.email;

        this.props.navigation.push('Dashboard',{
          apikey: 1,
          token: responseJson.data.auth.token,
          access: responseJson.data.auth.access,
          offset: 0,
          limit: 10
        });
      }else{
        alert('Invalid Email or Password!');
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {
    const version = "1.0.0";
    return (
      <ImageBackground source={require('./assets/img/bg.png')} style={styles.imgWrapper}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <KeyboardAvoidingView behavior="padding" style={{height: 230}}>
            <View style={styles.contentWrapper}>
              <View style={{alignSelf: 'stretch', justifyContent: 'center', alignItems: 'center'}}>
                <View style={{alignItems: 'center'}}>
                  <Text style={styles.textTitle}>Filter and Industrial</Text>
                </View>
              </View>
              <View style={styles.containerView}>
                <StatusBar
                  backgroundColor="#fff"
                  barStyle="light-content"
                />
                <TextInput placeholder="EMAIL ADDRESS" 
                  style={styles.input}
                  returnKeyType="next"
                  onSubmitEditing={()=>this.passwordInput.focus()}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  autoCorrect={false}
                  ref={_email => this._email = _email}
                />
                <TextInput placeholder="PASSWORD" 
                  style={styles.input2}
                  returnKeyType="go"
                  secureTextEntry
                  ref={_password => this._password = _password}
                />
                <TouchableOpacity style={styles.buttonContainer} onPress={this._signInAsync}>
                  <Text style={styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>

          <Button block style={{height: 35, padding: 5, marginTop: 30, marginBottom: 15, marginHorizontal:15, backgroundColor: '#fff', borderRadius: 15, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8,shadowRadius: 2, elevation: 3}} onPress={() => { this.props.navigation.navigate('Registerx') }}>
            <Row>
              <Col style={{justifyContent: 'center'}}>
                  <Text style={{textAlign: 'left', color: '#000', paddingLeft: 15}}>REGISTER ACCOUNT</Text>
              </Col>
              <View style={{alignItems:'flex-end', justifyContent: 'center' }}>
                  <Icon type="FontAwesome" name='chevron-right' style={{color: '#FF0000', fontSize: 16 }}/>
              </View>
            </Row>
          </Button>
          <Button block style={{marginHorizontal:15, height: 35, padding: 5, backgroundColor: '#fff', borderRadius: 15, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8,shadowRadius: 2, elevation: 3}} onPress={() => { this.props.navigation.navigate('ForgotPasswordx') }}>
            <Row>
              <Col style={{justifyContent: 'center'}}>
                  <Text style={{textAlign: 'left', color: '#000', paddingLeft: 15}}>FORGOT PASSWORD</Text>
              </Col>
              <View style={{alignItems:'flex-end', justifyContent: 'center'}}>
                  <Icon type="FontAwesome" name='chevron-right' style={{color: '#FF0000', fontSize: 16 }}/>
              </View>
            </Row>
          </Button>

        </View>
        <View style={{justifyContent: 'center', alignItems: 'center', paddingBottom: 15}}>
          <Text style={{fontSize: 12}}>Version: {version}</Text>
        </View>
      </ImageBackground>
    );
  }
  _signInAsync = async () => {
    responsexx = this.handleClick();
  };
}


class forgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading: true, 
        emailAdd:'',
        sentEmail: '',
        errorCode: ''
    }
  }

  sendForgotPassword = () => {
    const apikey = 1;
    const email = this.state.emailAdd;

    fetch('https://filterandindustrial.com.au/apix/member/forgot/', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          apikey:apikey,
          email: email  

      }),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.code != '0'){
         this.setState({errorCode: 'error'});
      }else{
        this.setState({sentEmail: 'sentEmail'})
      }
    })
    .catch((error) => {
      console.error(error);
    })
  }

  render() {
    return (
      <ImageBackground source={require('./assets/img/bg.png')} style={styles.imgWrapper}>
        <Container style={styles.container} >
          <Header transparent style={styles.header}>
            <Left>
              <Button transparent iconLeft style={{width: 200, height: 40}} 
               onPress={() => { this.props.navigation.navigate('SignInScreenx'); }} >
                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                  <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                  <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>FORGOT PASSWORD</Title>
                </View>
              </Button> 
            </Left>        
            <Right>
              <Button iconRight style={(this.state.sentEmail == 'sentEmail') ?  {display: 'none'} : styles.buttonRight } onPress={this.sendForgotPassword}>
                <View style={{flex:1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', textALign: 'center'}}>
                  <Text style={{fontWeight: 'bold', color: '#fff', paddingLeft: 15}}>SEND</Text>
                  <Icon type='FontAwesome' name='chevron-right' style={{fontSize: 12, color: '#fff', paddingLeft: 15}}/>
                </View>
              </Button>
            </Right>
          </Header>
          <View style={(this.state.sentEmail == 'sentEmail') ? {display: 'none'} : {paddingHorizontal: 15, paddingBottom: 10}}>
            <Text style={{fontSize: 12}}>Please enter your email address. You will receive a link to create a new password via email.</Text>
          </View>
          <View style={(this.state.errorCode == 'error') ? {paddingHorizontal: 15, paddingBottom: 20} : {display: 'none'}}>
            <Text style={{fontSize: 12, color: '#FF0000'}}>*Invalid email address.</Text>
          </View>
          <View style={(this.state.sentEmail == 'sentEmail') ? {display: 'none'} : {paddingBottom: 10, paddingHorizontal: 10}}>
            <Item rounded style={{height: 35, borderRadius:5, backgroundColor: '#fff', marginBottom: 20, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5}}>
              <TextInput
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false} 
                style={(this.state.errorCode == 'error') ? {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#FF0000', paddingHorizontal: 10, borderRadius: 5} : {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#fff', paddingHorizontal: 10, borderRadius: 5}}  
                placeholderTextColor="#C1C1C1" 
                placeholder="EMAIL ADDRESS" onChangeText={(emailAdd) => this.setState({emailAdd, errorCode: ''})} 
                value={this.state.emailAdd} 
              />
            </Item>
          </View>
         <View style={(this.state.sentEmail == 'sentEmail') ? {margintop: 30, marginHorizontal: 30, paddingBottom: 10, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center', height: 150, backgroundColor: '#FF0000'} : {display: 'none'}}>
            <Text style={{color: '#fff'}}>Link has been sent to your email. Please check your email and follow the instruction.</Text>
         </View>
        </Container>
      </ImageBackground>
    );
  }
}


class register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading: true, 
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: '',
        reqFname: '',
        reqLname: '',
        reqEmail: '',
        reqPassword: '',
        reqConfirmPassword: '',
    }
  }



  handleClickRegister = () => {
    const apikey = 1;
    let fName = this.state.firstName;
    let lName = this.state.lastName;
    let email = this.state.email;
    let password = this.state.password;
    let confirmPassword = this.state.confirmPassword;
    
    if(password != confirmPassword){
      this.setState({reqConfirmPassword: 'required'});
      return false;
    }
    if(fName == ''){
      this.setState({reqFname: 'required'});
      return false;
    }
    if(lName == ''){
      this.setState({reqLname: 'required'});
      return false;
    }
    if(email == ''){
      this.setState({reqEmail: 'required'});
      return false;
    }
    if(password == ''){
      this.setState({reqPassword: 'required'});
      return false;
    }
    fetch('https://filterandindustrial.com.au/apix/member/sbmitRegister', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        apikey:apikey,
        fname:fName,
        lname:lName,
        email:email,  
        password:password
      }),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.code != '0'){
        this.setState({reqEmail: 'required'});
      }else{
        alert('Registration Complete');
      }
    })
    .catch((error) => {
      console.error(error);
    })
  }

  render() {
    return (
      <ImageBackground source={require('./assets/img/bg.png')} style={styles.imgWrapper}>
        <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <Container style={styles.container}>
          <Header transparent style={styles.header}>
            <Left>
              <Button transparent iconLeft style={{width: 200, height: 40}} 
                onPress={() => { this.props.navigation.navigate('SignInScreenx'); }} >
                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                  <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                  <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>REGISTER</Title>
                </View>
              </Button> 
            </Left>        
            <Right>
              <Button iconRight style={styles.buttonRight} onPress = {this._registerInAsync}>
                <View style={{flex:1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', textALign: 'center'}}>
                  <Text style={{fontWeight: 'bold', color: '#fff', paddingLeft: 15}}>REGISTER</Text>
                  <Icon type='FontAwesome' name='chevron-right' style={{fontSize: 12, color: '#fff', paddingLeft: 10}}/>
                </View>
              </Button>
            </Right>
          </Header>
          <View style={{paddingHorizontal: 15, paddingBottom: 10}}>
            <Text style={{fontSize: 12}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim leo, fermentum in lacinia nec, iaculis eget libero</Text>
          </View>
          <View style={{paddingHorizontal: 15, paddingBottom: 20,}}>
            <Text style={(this.state.reqFname == 'required') ? {color: '#FF0000', fontSize: 12,} : {display: 'none'}}>*Firstname is required</Text>
            <Text style={(this.state.reqLname == 'required') ? {color: '#FF0000', fontSize: 12,} : {display: 'none'}}>*Lastname is required</Text>
            <Text style={(this.state.reqEmail == 'required') ? {color: '#FF0000', fontSize: 12,} : {display: 'none'}}>*Invalid email address.</Text>
            <Text style={(this.state.reqPassword == 'required') ? {color: '#FF0000', fontSize: 12,} : {display: 'none'}}>*Password is required</Text>
            <Text style={(this.state.reqConfirmPassword == 'required') ? {color: '#FF0000', fontSize: 12,} : {display: 'none'}}>*Confirm password does not match</Text>
          </View>
          <View style={{paddingHorizontal: 10, height: 45}}>
            <Row>
              <Col style={{paddingRight: 5,}}>
                <Item rounded style={{height: 35, borderRadius:5, backgroundColor: '#fff', marginBottom: 10, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5}}>
                  <TextInput
                    onFocus = {() => {this.setState({reqFname:''})}}
                    returnKeyType="next" 
                    style={(this.state.reqFname == 'required') ? {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#FF0000', paddingHorizontal: 10, borderRadius: 5} : {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#fff', paddingHorizontal: 10, borderRadius: 5}} 
                    placeholderTextColor="#C1C1C1" 
                    placeholder="FIRST NAME" 
                    onChangeText={(firstName) => this.setState({firstName, reqFname:''})} 
                    value={this.state.firstName} 
                  />
                </Item>
              </Col>
              <Col style={{paddingLeft: 5,}}>
                <Item rounded style={{height: 35, borderRadius:5, backgroundColor: '#fff', marginBottom: 10, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5}}>
                  <TextInput
                    onFocus = {() => {this.setState({reqLname:''})}}
                    returnKeyType="next" 
                    style={(this.state.reqLname == 'required') ? {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#FF0000', paddingHorizontal: 10, borderRadius: 5} : {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#fff', paddingHorizontal: 10, borderRadius: 5}} 
                    placeholderTextColor="#C1C1C1" 
                    placeholder="LAST NAME" 
                    onChangeText={(lastName) => this.setState({lastName, reqLname:''})} 
                    value={this.state.lastName} 
                  />
                </Item>
              </Col>
            </Row>
          </View>
          <View style={{paddingHorizontal: 10}}>
            <Item rounded style={{height: 35, borderRadius:5, backgroundColor: '#fff', marginBottom: 10, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5}}>
              <TextInput
                onFocus = {() => {this.setState({reqEmail:''})}}
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType="next" 
                style={(this.state.reqEmail == 'required') ? {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#FF0000', paddingHorizontal: 10, borderRadius: 5} : {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#fff', paddingHorizontal: 10, borderRadius: 5}} 
                placeholderTextColor="#C1C1C1" 
                placeholder="EMAIL ADDRESS" 
                onChangeText={(email) => this.setState({email, reqEmail:''})} 
                value={this.state.email} 
              />
            </Item>
          </View>
          <View style={{paddingHorizontal: 10}}>
            <Item rounded style={{height: 35, borderRadius:5, backgroundColor: '#fff', marginBottom: 10, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5}}>
              <TextInput
                onFocus = {() => {this.setState({reqPassword:''})}}
                secureTextEntry
                returnKeyType="next" 
                style={(this.state.reqPassword== 'required') ? {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#FF0000', paddingHorizontal: 10, borderRadius: 5} : {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#fff', paddingHorizontal: 10, borderRadius: 5}} 
                placeholderTextColor="#C1C1C1" 
                placeholder="PASSWORD" 
                onChangeText={(password) => this.setState({password, reqPassword:''})} 
                value={this.state.password} 
              />
            </Item>
          </View>
          <View style={{paddingHorizontal: 10}}>
            <Item rounded style={{height: 35, borderRadius:5, backgroundColor: '#fff', marginBottom: 10, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5}}>
              <TextInput
                onFocus = {() => {this.setState({reqConfirmPassword:''})}}
                secureTextEntry
                returnKeyType="go" 
                style={(this.state.reqConfirmPassword == 'required') ? {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#FF0000', paddingHorizontal: 10, borderRadius: 5} : {flex: 1, height: 35, borderWidth: 0.5, borderColor: '#fff', paddingHorizontal: 10, borderRadius: 5}} 
                placeholderTextColor="#C1C1C1" 
                placeholder="CONFIRM PASSWORD" 
                onChangeText={(confirmPassword) => this.setState({confirmPassword, reqConfirmPassword:''})} 
                value={this.state.confirmPassword} 
              />
            </Item>
          </View>
        </Container>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
  _registerInAsync = async () => {
    handlex = this.handleClickRegister();
  };
}


class Dashboardx extends React.Component {
  render() {
    return (
      <Dashboard/>
    );
  }
}

class DesignScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    StatusBar.setHidden(true);
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render(){
    if (this.state.loading){
      return(
        <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
          <Spinner color='red' />
        </View>
      );
    }
     return (
         <ViewKitDesign/>
      );
  }
}

const styles = StyleSheet.create({
  header:{
    paddingTop: 0,
    height: 50,
    alignItems: 'center',
  },
  iconLeft:{
      color: '#FF0000',
      fontSize: 16, 
      marginLeft: 0,
      ...Platform.select({
          android:{
              marginLeft: -7,
          }
      })
  },
  iconRight:{
      color: '#FF0000',
      fontSize: 16, 
      marginRight: 0,
      ...Platform.select({
          android:{
              marginRight: -7,
          }
      })
  },
  icon:{
    color: '#fff',
    fontSize: 18,
    backgroundColor: 'green'
  },
  buttonRight:{
    width: 100,
    backgroundColor: '#FF0000',
    borderRadius: 10,
    height: 30,
  },
  container:{
    flex: 1,
    backgroundColor: 'transparent'
  },
  imgWrapper: {
    flex: 1,
  },
  contentWrapper:{
    flex: 1,
    //flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  textTitle:{
    fontSize: 30,
    fontWeight: 'bold',
    width: 320,
    textAlign: 'center'

  },
  containerView:{
    paddingTop: 15,
    alignItems: 'center',
  },
  input2:{
    width:250,
    height: 40,
    backgroundColor: '#fff',
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  input:{
    width:250,
    height: 40,
    backgroundColor: '#fff',
    marginBottom: 20,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  buttonContainer:{
     backgroundColor: '#FF0000',
     width: 100,
     borderRadius: 20
  },
  buttonText:{
    textAlign: 'center',
    color: '#fff',
    paddingVertical: 10,
  }

});

const Dashx = createStackNavigator({ 
  Dashboard: {
    screen: Dashboard,
    navigationOptions:{
      header: null
    }
  } 
});
const Loginx = createStackNavigator({ 
  SignInScreen: {
    screen: SignInScreen,
    navigationOptions:{
      header: null
    }
  }  
});

const forgotPasswordx = createStackNavigator({
  ForgotPassword: {
    screen: forgotPassword,
    navigationOptions:{
      header: null
    }
  } 
});

const registerx = createStackNavigator({
  Register: {
    screen: register,
    navigationOptions:{
      header: null
    }
  } 
});

const Designx = createStackNavigator({ 
  DesignScreen: {
    screen: DesignScreen,
    navigationOptions:{
      header: null
    }
  }  
});
export default createAppContainer(createSwitchNavigator(
  {
    DashboardScreenx: Dashx,
    SignInScreenx: Loginx,
    ForgotPasswordx: forgotPasswordx,
    Registerx: registerx,
    DesignScreenx: Designx,
  },
  {
    initialRouteName: 'SignInScreenx',
  }
));