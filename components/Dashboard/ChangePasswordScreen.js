import React, {Component} from 'react';
import { Platform, StyleSheet,ImageBackground, View, TextInput} from 'react-native';
import { Container, Content, Button, Icon, Text, Header, Left, Right, Form, Title, Item} from 'native-base';
import { withNavigation } from 'react-navigation';

//import Header from './DashboardComponents/Dashboard/Header';

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true, 
            newPassword:"",
            confirmNewPassword:""
        }
    }

    _handleClick = () => {
        const apikey = 1;
        const token =  tokenx;
        const access = accessx;
        const _newPassword = this.state.newPassword;
        const _confirmPassword = this.state.confirmNewPassword;

        fetch('https://filterandindustrial.com.au/apix/member/changepassword', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ apikey:apikey,
                apikey: apikey,
                token: token,
                access: access,
                password: _newPassword,
                coPassword: _confirmPassword
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            //check for errors
            if(responseJson['code'] != 0 ){
                alert(responseJson['message']);
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                    this.props.navigation.push('SignInScreen');
                    return false;
                }
            }else{
                alert('Password has been updated!');
                this.props.navigation.push('Account');
            }
        })
        .catch((error) => {
            console.error(error);
        })
      
    }

    render() {
        return (
            <ImageBackground source={require('./../../assets/img/bg.png')} style={styles.imgWrapper}>
                <Container style={styles.container}>
                    <Header transparent style={styles.header}>
                        <Left>
                           <Button transparent iconLeft style={{width: 200, height: 40}} onPress={() => { this.props.navigation.goBack() }}>
                                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                    <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                                    <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>CHANGE PASSWORD</Title>
                                </View>
                            </Button>
                        </Left>
                        <Right>
                            <Button iconRight style={styles.button} onPress={this._Async}>
                                <Text style={styles.text}>UPDATE</Text>
                            </Button>
                        </Right>
                    </Header>
                    <View style={{paddingHorizontal: 10, paddingBottom: 20}}>
                        <View>
                          <Text style={{fontSize: 12}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim leo, fermentum in lacinia nec, iaculis eget libero</Text>  
                        </View>
                    </View>
                    <Content style={{flex: 1, paddingHorizontal: 10}}>
                        <Form>
                            <Item rounded style={styles.inputContainer}>
                                <TextInput 
                                    placeholderTextColor="#C1C1C1" 
                                    placeholder='NEW PASSWORD' 
                                    style={styles.input}
                                    secureTextEntry 
                                    onChangeText={(newPassword) => this.setState({newPassword})} 
                                />
                            </Item>
                            <Item rounded style={styles.inputContainer}>
                                <TextInput 
                                    placeholderTextColor="#C1C1C1" 
                                    placeholder='CONFIRM NEW PASSWORD'
                                    secureTextEntry 
                                    style={styles.input} 
                                    onChangeText={(confirmNewPassword) => this.setState({confirmNewPassword})} 
                                />
                            </Item>
                        </Form>
                    </Content>
                </Container>
            </ImageBackground>
        );
    }
    _Async = async () => {
        responsex = this._handleClick();
    };
}

export default withNavigation(ChangePassword);

const styles = StyleSheet.create({
    header:{
        paddingTop: 0,
        height: 50,
        alignItems: 'center',
    },
    iconLeft:{
        color: '#FF0000',
        fontSize: 16, 
        marginLeft: 0,
        ...Platform.select({
            android:{
                marginLeft: -7,
            }
        })
    },
    iconRight:{
        color: '#FF0000',
        fontSize: 16, 
        marginRight: 0,
        ...Platform.select({
            android:{
                marginRight: -7,
            }
        })
    },
    button:{
      backgroundColor: '#FF0000',
      borderRadius: 10,
      height: 30,
    },
    imgWrapper: {
        flex: 1
    },
    container:{
        backgroundColor: 'transparent'
    },
    headerWrapper:{
        backgroundColor: '#fff'
    },
    contentWrapper:{
        //backgroundColor: 'transparent'
    },
    footerWrapper:{
        backgroundColor: '#FF0000'
    },
    buttonWrapper:{
        backgroundColor: '#FF0000'
    },
    badgeLogo:{
        height: 25,
        width: 25,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textLogo:{
        fontWeight: 'bold',
        fontSize: 12
    },
    textName:{
        color: '#000',
        fontSize: 18,
        fontWeight: 'bold'
    },
    textTitle:{
        color: '#000',
        fontWeight: 'bold'
    },
    iconIcon:{
        color: '#FF0000',
        fontSize: 21
    },
    iconIcon2:{
        color: '#FF0000',
        fontSize: 26
    },
    icon:{
        color: '#fff',
        fontSize: 18
    },
    inputContainer:{
        backgroundColor: '#fff',
        borderRadius: 5,
        marginBottom: 15
    },
    input:{
        paddingHorizontal: 10,
        flex: 1,
        borderWidth: 0,
        height: 30
    }
});