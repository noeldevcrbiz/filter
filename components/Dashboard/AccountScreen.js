import React, {Component} from 'react';
import { Platform, StyleSheet,ImageBackground, View} from 'react-native';
import { Container, Content, Button, Icon, Text, Header, Left, Body, Right, Title, Card, CardItem, Badge, Row, Col} from 'native-base';
import { withNavigation } from 'react-navigation';

//import Header from './DashboardComponents/Dashboard/Header';

class Account extends Component {
    render() { 
        return (
            <ImageBackground source={require('./../../assets/img/bg.png')} style={styles.imgWrapper}>
                <Container style={styles.container}>
                    <Header transparent style={styles.header}>
                        <Left>
                           <Button transparent iconLeft style={{width: 200, height: 40}} onPress={() => { this.props.navigation.push('Dashboard') }}>
                                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                    <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                                    <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>ACCOUNT</Title>
                                </View>
                            </Button>
                        </Left>
                        <Right>

                        </Right>
                    </Header>
                    <View style={{paddingHorizontal: 10, paddingBottom: 20}}>
                        <View>
                          <Text style={{fontSize: 12}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim leo, fermentum in lacinia nec, iaculis eget libero</Text>  
                        </View>
                    </View>
                    <Content style={{flex: 1, paddingHorizontal: 10}}>
                        <Card style={{borderRadius: 10, marginBottom: 15}}>
                            <CardItem style={{borderRadius: 10, padding: 10}}>
                                <Body>
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', paddingBottom: 15}}>
                                        <View>
                                            <Badge style={styles.badgeLogo}>
                                                <Text style={styles.textLogo}>{firstLetterx}</Text>
                                            </Badge>
                                        </View>
                                        <View style={{paddingLeft: 10}}>
                                            <Text style={styles.textName}>{fNamex} {lNamex}</Text>
                                        </View>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', paddingBottom: 15}}> 
                                        <View>
                                            <View style={styles.badgeLogo}>
                                                <Icon type="FontAwesome" name='envelope' style={styles.iconIcon}/>
                                            </View>
                                        </View>
                                        <View style={{paddingLeft: 10}}>
                                            <Text style={styles.textName}>{emailx}</Text>
                                        </View>
                                    </View>
                                     <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                        <View>
                                            <View style={styles.badgeLogo}>
                                                <Icon type="FontAwesome" name='lock' style={styles.iconIcon2}/>
                                            </View>
                                        </View>
                                        <View style={{paddingLeft: 10}}>
                                            <Text style={styles.textName}>************</Text>
                                        </View>
                                    </View>
                                </Body>
                            </CardItem>
                        </Card>
                        <View style={{paddingBottom: 50, flex: 1}}>
                            <Button block style={{backgroundColor: '#fff', borderRadius: 5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8,shadowRadius: 2, elevation: 3}} onPress={() => { this.props.navigation.navigate('ChangePassword') }}>
                                <Row>
                                    <Col style={{justifyContent: 'center'}}>
                                        <Text style={{textAlign: 'left', color: '#000'}}>CHANGE PASSWORD</Text>
                                    </Col>
                                    <View style={{alignItems:'flex-end', justifyContent: 'center'}}>
                                        <Icon type="FontAwesome" name='chevron-right' style={{color: '#FF0000', fontSize: 16 }}/>
                                    </View>
                                </Row>
                            </Button>
                        </View>
                    </Content>
                </Container>
            </ImageBackground>
        );
    }
}

export default withNavigation(Account);

const styles = StyleSheet.create({
    header:{
        paddingTop: 0,
        height: 50,
        alignItems: 'center',
    },
    iconLeft:{
        color: '#FF0000',
        fontSize: 16, 
        marginLeft: 0,
        ...Platform.select({
            android:{
                marginLeft: -7,
            }
        })
    },
    iconRight:{
        color: '#FF0000',
        fontSize: 16, 
        marginRight: 0,
        ...Platform.select({
            android:{
                marginRight: -7,
            }
        })
    },
    imgWrapper: {
        flex: 1
    },
    container:{
        backgroundColor: 'transparent'
    },
    headerWrapper:{
        backgroundColor: '#fff'
    },
    contentWrapper:{
        //backgroundColor: 'transparent'
    },
    footerWrapper:{
        backgroundColor: '#FF0000'
    },
    buttonWrapper:{
        backgroundColor: '#FF0000'
    },
    badgeLogo:{
        height: 25,
        width: 25,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textLogo:{
        fontWeight: 'bold',
        fontSize: 12
    },
    textName:{
        color: '#000',
        fontSize: 18,
        fontWeight: 'bold'
    },
    textTitle:{
        color: '#000',
        fontWeight: 'bold'
    },
    iconIcon:{
        color: '#FF0000',
        fontSize: 21
    },
    iconIcon2:{
        color: '#FF0000',
        fontSize: 26
    },
    icon:{
        color: '#000',
        fontSize: 18
    }
});