import React, {Component} from 'react';
import { StyleSheet, ImageBackground, View, ActivityIndicator, FlatList, List, ListItem, Separator, TouchableOpacity, Alert, Image, Linking, Platform} from 'react-native';
import { Container, Footer, FooterTab, Content, Card, CardItem, Text, Body, Button, Icon, Badge, Left, Right, Row, Col, Spinner} from 'native-base';
import { Menu, MenuOptions, MenuOption,MenuTrigger, MenuProvider } from 'react-native-popup-menu';
import { withNavigation } from 'react-navigation';

import DashboardHeader from './DashboardComponents/Dashboard/Header';
import AddKitButton from './DashboardComponents/Dashboard/AddtokitButton';

class Dashboard extends Component {

    constructor(props) {
        super(props);
          this.state = {
            isLoading: true, 
            dataSource: [],
            page:0,
            loadingMore: false,
            loading: true,
            error: null,
            refreshing:false
        }

        navFrom = 'dashBoard';
    }

    componentWillMount(){
        this._fetchAllKits();
    }

    _fetchAllKits(){
        const  page  = this.state.page;
        const { navigation } = this.props;
        const apikey = 1;
        const token = tokenx;
        const access = accessx;
        const limit = navigation.getParam('limit', 10);

        fetch('https://filterandindustrial.com.au/apix/Kits/View', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: apikey,
                token: token,
                access: access,
                offset: page * 10,
                limit: limit
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {

            //check for errors
            if(responseJson['code'] != 0 ){
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                    alert(responseJson['message']);
                    this.props.navigation.push('SignInScreen');
                    return false;
                }
            }

            this.setState({
              isLoading: false,
              dataSource: [...this.state.dataSource,...responseJson.data['items']],
              refreshing:false
            })
        })
        .catch((error) => {
          console.error(error);
        })
    }

    _handleLoadMore = () => {
        this.setState(
          (prevState, nextProps) => ({
            page: prevState.page + 1,
            loadingMore: true
          }),
          () => {
            this._fetchAllKits();
          }
        )
    };

    _setKitGlobalID(id){
        global.kitIDx = id;
        this.props.navigation.push('ViewKit');
    };

    _editKit(id){
        global.kitIDx = id;
        this.props.navigation.push('EditKit', {fromnav: 'dashBoard'});
    }

    
    removeItemValue(key,item) {
        fetch('https://filterandindustrial.com.au/apix/Kits/delete', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: 1,
                token: tokenx,
                access: accessx,
                kitid: item
              
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            //check for errors
            if(responseJson['code'] != 0 ){
                alert(responseJson['message']);
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                    this.props.navigation.push('SignInScreen');
                    return false;
                }
            }

            var dataSource = this.state.dataSource;
            dataSource.splice(key, 1);

            if(dataSource.length==0){
                this.setState({ dataSource:[]});
            }else{
                this.setState({ dataSource });
            }
              alert( 
                'Kit has been deleted',
                '' ,
                [
                    {
                        text: 'OK', 
                        onPress: () => console.log('Delete Success!')
                    },
                    {
                        cancelable: false
                    },
                ]
            )
        })
        .catch((error) => {
          console.error(error);
        })      

    };

    //if list is empty
    ListEmptyView = () => {
        return (
            <View style={{flex: 1}}>
                <Text style={{textAlign: 'center'}}>No Kit to Display</Text>
            </View>
        );
    };

    //call
    dialCall = (number) => {
        let phoneNumber = '';
        if (Platform.OS === 'android'){ 
            phoneNumber = `tel:${number}`; 
        }else {
            phoneNumber = `telprompt:${number}`; 
        }
        Linking.openURL(phoneNumber);
    };

    render() {
       if(this.state.isLoading){
          return(
            <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
              <Spinner color='red' />
            </View>
          )
        }

        return (
            <ImageBackground source={require('./../../assets/img/bg.png')} style={styles.imgWrapper}>
                    <Container style={styles.container}>
                        <DashboardHeader/> 
                        <View style={{backgroundColor: '#000', height: 35, paddingHorizontal: 15}}>
                            <Row>
                                <Col style={{justifyContent: 'center'}}>
                                    <Text style={{color: '#fff', fontWeight: 'bold'}}>Filter and Industrial</Text>
                                </Col>
                                <Col style={{justifyContent: 'center', alignItems: 'flex-end'}}>
                                    <TouchableOpacity
                                        style={{ height: 25,backgroundColor: "#FF0000", alignItems: "center", justifyContent: "center", borderRadius: 10, paddingHorizontal: 10}}
                                        onPress={()=>{this.dialCall(1800330318)}}
                                    >
                                        <Text style={{color: '#fff'}}>
                                           <Text>
                                               <Icon style={{color: '#fff', fontSize: 16}} type="FontAwesome" name="phone" />
                                            </Text>
                                           <Text style={{color: '#fff'}}> 1800 330 318</Text>
                                        </Text> 
                                    </TouchableOpacity>
                                </Col>
                            </Row>
                       </View>   
                        <MenuProvider customStyles={menuProviderStyles}>
                           <View style={{flex: 1}}>
                                <View style={{flexDirection:'row', justifyContent:'flex-end', paddingHorizontal: 15, paddingTop: 30}}>
                                    <AddKitButton style={{alignSelf: 'flex-end'}} />
                                </View>
                                <View style={styles.titleContainer}>
                                    <Icon type='FontAwesome'name="circle" style={styles.circleIcon}/>
                                    <Text style={styles.text}>KITS</Text>
                                </View>
                                    <FlatList
                                        data={this.state.dataSource}
                                        extraData={this.state}  
                                        renderItem={
                                            ({item, index}) =>
                                            <View style={{paddingBottom: 25, paddingHorizontal: 10}}>
                                                <TouchableOpacity onPress={() => { this._setKitGlobalID(item.id) }}>
                                                    <Card style={styles.cardContainer}>                     
                                                        <CardItem header style={{paddingBottom: 0, backgroundColor: 'transparent'}}>
                                                            <Row style={{zIndex: 0}}>
                                                                <Col>
                                                                    <Row>
                                                                        <View style={{paddingRight: 5}}> 
                                                                            <Badge style={styles.badgeCircle}>
                                                                                <Text style={styles.textCircle}>{item.id}</Text>
                                                                            </Badge>
                                                                        </View>
                                                                        <Col style={{paddingRight: 25}}>
                                                                            <Text style={styles.titleText}>{item.boomTitle}</Text>
                                                                            <Text style={styles.dateText}>{item.datex}</Text>
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                            </Row>
                                                        </CardItem>
                                                        <CardItem style={{paddingTop: 10, backgroundColor: 'transparent'}}>
                                                            <Body>
                                                                <Text style={{color: '#828080', fontSize: 14}}>{item.f_description}</Text>
                                                            </Body>
                                                        </CardItem>
                                                        <Menu style={{position: 'absolute', top: 0, right: 10}}>
                                                            <MenuTrigger style={{alignItems: 'flex-end', justifyContent: 'center', height: 30, paddingRight: 10, width: 50}}>
                                                                <Icon type="FontAwesome" name="ellipsis-h" style={{color: '#FF0000', fontSize: 18}}/>
                                                            </MenuTrigger>
                                                            <MenuOptions customStyles={optionsStyles}>
                                                                <MenuOption style={{paddingVertical: 0}} onSelect={() => this._editKit(item.id) }>
                                                                    <Text style={{color:'#fff', fontSize: 18}}>Edit</Text>
                                                                </MenuOption>
                                                                <MenuOption style={{paddingVertical: 0}} onSelect={() => this._setKitGlobalID(item.id) }>
                                                                    <Text style={{color:'#fff', fontSize: 18}}>View</Text>
                                                                </MenuOption>
                                                                <MenuOption style={{paddingVertical: 0}}
                                                                    onSelect = {()=>{ 
                                                                        Alert.alert(
                                                                            'Confirm Delition', 
                                                                            'Are you sure to delete this kit?' ,
                                                                            [
                                                                                {
                                                                                    text: 'Cancel', 
                                                                                    onPress: () => console.log('Cancel Pressed'), 
                                                                                    style: 'cancel', 
                                                                                },
                                                                                {
                                                                                    text: 'Delete', 
                                                                                    onPress: () => this.removeItemValue(index,item.id)
                                                                                },
                                                                                {
                                                                                    cancelable: false
                                                                                },
                                                                            ]
                                                                        )
                                                                    }}
                                                                >
                                                                    <Text style={{color:'#fff', fontSize: 18}}>Delete</Text>
                                                                </MenuOption>
                                                            </MenuOptions>
                                                        </Menu>
                                                    </Card>
                                                </TouchableOpacity>
                                            </View>
                                        }
                                        keyExtractor={item => item.id}
                                        onEndReached={this._handleLoadMore}
                                        onEndReachedThreshold={0.5}
                                        initialNumToRender={1}
                                        refreshing={this.state.refreshing}
                                        ListEmptyComponent={this.ListEmptyView}
                                    />
                            </View>
                        </MenuProvider>
                    </Container>
            </ImageBackground>
        );
    }
}

export default Dashboard;


const optionsStyles = {
    optionsContainer: {
        backgroundColor: '#FF0000',
        borderRadius: 10, 
        width: 90, 
        height: 92, 
        padding:5,
        marginTop: 25,
    }
};

const styles = StyleSheet.create({
    menuWrapper:{
        backgroundColor: 'transparent'
    },
    imgWrapper: {
        flex: 1
    },
    container:{
        backgroundColor: 'transparent'
    },
    headerWrapper:{
        backgroundColor: '#fff'
    },
    contentWrapper:{
        //backgroundColor: 'transparent'
    },
    footerWrapper:{
        backgroundColor: '#FF0000'
    },
    buttonWrapper:{
        backgroundColor: '#FF0000'
    },
    icon:{
        color: '#fff'
    },
    text:{
        color: '#fff'
    },

    buttonKitContainer:{
        paddingVertical: 15,
        flexDirection: 'row-reverse'
    },
    titleContainer:{
        flexDirection: 'row',
        padding: 15
    },

    cardContainer:{
       borderRadius: 10,
       minHeight: 150
    },
    circleIcon:{
        color: '#FF0000',
        paddingRight: 10,
        paddingTop: 5,
        fontSize: 14
    },
    button:{
        backgroundColor: '#fff',
        borderRadius: 20
    },
    iconLight:{
        fontWeight: '100'
    },
    text:{
        fontWeight: 'bold'
    },
    badgeCircle:{
        height: 30,
        width: 30,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textCircle:{
        fontSize: 8,
        fontWeight: 'bold',
        width: 30,
        
    },

    titleText:{
       fontSize: 12
    },

    dateText:{
        fontSize: 9,
        color: '#828080'
    }
});

const menuProviderStyles = {
    menuProviderWrapper: styles.menuWrapper,
}