import React, {Component} from 'react';
import { Platform, StyleSheet, ImageBackground, View} from 'react-native';
import { Container, Content, Card, CardItem, Text, Body, Button, Icon, Badge, Left, Right, Row, Col} from 'native-base';
import { withNavigation } from 'react-navigation';

class AddtokitButton extends React.Component {
	
	render() {
		return (
		  	<Button light iconLeft style={styles.button} onPress={() => this.props.navigation.navigate('AddKit')}>
		        <Icon type='FontAwesome' name='plus' style={styles.iconLight} />
		        <Text>Add Kit</Text>
		    </Button>
		);
	}
}
export default withNavigation(AddtokitButton);

const styles = StyleSheet.create({

    button:{
        backgroundColor: '#fff',
        borderRadius: 20
    },
    iconLight:{
        fontWeight: '100'
    }    
});