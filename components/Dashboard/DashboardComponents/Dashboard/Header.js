import React, {Component} from 'react';
import { StyleSheet,ImageBackground, View, Alert} from 'react-native';
import { Header, Left, Body, Right, Button, Icon, Title, Text, Badge, Grid, Col, Row} from 'native-base';
import {withNavigation} from 'react-navigation';

class HeaderContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            screenName: ''
        }  
    };

     componentWillMount(){
        let screenName = this.props.navigation.state.routeName
        this.setState({screenName:screenName});
    }

    render() {
        return (
            <Header style={styles.headerWrapper}>
                <Left>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <View>
                            <Badge style={styles.badgeLogo}>
                                <Text style={styles.textLogo}>{firstLetterx}</Text>
                            </Badge>
                        </View>
                        <View style={{width: 200, paddingLeft: 10}}>
                            <Text style={styles.textName}>{lNamex}, {fNamex}</Text>
                            <Text style={styles.textTitle}>{this.state.screenName}</Text>
                        </View>
                    </View>
                </Left>
                <Right>
                    <Button 
                        transparent 
                        onPress={() => 
                            Alert.alert(
                                'Confirm Sign out', 
                                'Are you signing out?' ,
                                [
                                    {
                                        text: 'Cancel', 
                                        onPress: () => console.log('Cancel Pressed'), 
                                        style: 'cancel', 
                                    },
                                    {
                                        text: 'Yes', 
                                        onPress: () =>this.props.navigation.push('SignInScreen')
                                    },
                                    {
                                        cancelable: false
                                    },
                                ]
                            )

                        }
                    >
                        <Icon type="FontAwesome" name="sign-out" style={styles.icon}/>
                    </Button>
                </Right>
            </Header>
        );
    }
}

export default withNavigation(HeaderContent);

const styles = StyleSheet.create({

    headerWrapper:{
        backgroundColor: '#fff'
    },
    badgeLogo:{
        height: 40,
        width: 40,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textLogo:{
        fontWeight: 'bold',
        fontSize: 21
    },
    textName:{
        color: '#000',
        fontSize: 10,
    },
    textTitle:{
        color: '#000',
        fontWeight: 'bold'
    },
    icon:{
        color: '#000',
        fontSize: 21
    }
});