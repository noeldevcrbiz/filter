import React, {Component} from 'react';
import { StyleSheet,ImageBackground, View, Text} from 'react-native';
import { Ionicons } from '@expo/vector-icons'; // 6.2.2
import {Icon } from 'native-base';
import { createBottomTabNavigator, createAppContainer, createStackNavigator, createSwitchNavigator, StackNavigator} from 'react-navigation';


import DashboardScreen from './DashboardScreen';
import AccountScreen from './AccountScreen';
import AddKitScreen from './AddKitScreen';
import EditKitScreenx from './EditKitScreen';

import ViewKitScreen from './ViewKitScreen';
import RequestKitScreen from './RequestKitScreen';
import AddProductScreen from './AddProductScreen';
import SearchProductScreen from './SearchProductScreen';
import SearchEditProductScreen from './SearchEditProductScreen';
import EditProductScreen from './EditProductScreen';
import CopyKitScreen from './CopyKitScreen';

import ChangePasswordScreen from './ChangePasswordScreen';

const DashboardScreenx = createStackNavigator({
    Dashboard: {
        screen: DashboardScreen,
        navigationOptions:{
            header: null
        }
    },
    EditKit:{
        screen: EditKitScreenx,
        navigationOptions:{
            header: null
        }
    },
    ViewKit:{
        screen: ViewKitScreen,
        navigationOptions:{
            header: null
        }
    },
    RequestKit:{
        screen: RequestKitScreen,
        navigationOptions:{
            header: null
        }
    },
    AddProduct:{
        screen: AddProductScreen,
        navigationOptions:{
            header: null
        }
    },
    SearchProduct:{
        screen: SearchProductScreen,
        navigationOptions:{
            header: null
        }
    },
    SearchEditProduct:{
        screen: SearchEditProductScreen,
        navigationOptions:{
            header: null
        }
    },
    EditProduct:{
        screen: EditProductScreen,
        navigationOptions:{
            header: null
        }
    },
     CopyKit:{
        screen: CopyKitScreen,
        navigationOptions:{
            header: null
        }
    },
});

DashboardScreenx.navigationOptions = ({ navigation }) => {

    global.counter = 0;
    let tabBarVisible = true; 
    if (navigation.state.index > 1) {
        tabBarVisible = false;
    }
    return {
        tabBarVisible,
    };

};


const AccountScreenx = createStackNavigator({
    Account: {
        screen: AccountScreen,
        navigationOptions:{
            tabBarVisible: false,
            header: null
        }
    },
    ChangePassword:{
        screen: ChangePasswordScreen,
        navigationOptions:{
            tabBarVisible: false,
            header: null
        }
    }
});

const AddKitScreenx = createStackNavigator({
    AddKit: {
        screen: AddKitScreen,
        navigationOptions:{
            tabBarVisible: false,
            header: null
        }
    }
});


const MainTabs = createBottomTabNavigator({
    Home: {
        screen: DashboardScreenx,
        navigationOptions:() => ({
            tabBarVisible: true,
            header: null,
            tabBarIcon: ({tintColor}) => {
                return <Icon type="FontAwesome" name="home" color={tintColor} size={14}/>;
            }
        })
    },
    Account: {
        screen: AccountScreenx,
        navigationOptions:() => ({
            tabBarVisible: false,
            header: null,
            tabBarIcon: ({tintColor }) => {
                return <Icon type="FontAwesome" name="user" color={tintColor} size={14}/>;
            }
        })
    },
    AddKit: {
        screen: AddKitScreenx,
        navigationOptions:() => ({
            tabBarVisible: false,
            header: null,
            tabBarIcon: ({tintColor }) => {
                return <Icon type="FontAwesome" name="plus" color={tintColor} size={14}/>;
            }
        })
    }
},
{
    tabBarOptions:{
        activeTintColor: '#fff', 
        inactiveTintColor: '#000',
        style: {
            backgroundColor: '#FF0000',
        }
    }
}

);

export default createAppContainer(MainTabs);