import React, { Component } from 'react';
import {Platform, StyleSheet, View, FlatList, List, ListItem, Image, Separator, TouchableOpacity, Alert} from 'react-native';
import {Header, Left, Right, Button, Icon, Title, Container, Content, Text, Card, CardItem, Row, Col, Badge, Body, Spinner} from 'native-base';
import { CheckBox } from 'react-native-elements'
import { withNavigation } from 'react-navigation';
import KitDropDownMenu from './KitDropDown';


class ViewKitContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true, 
            dataSource: [],
            page:0,
            loadingMore: false,
            loading: true,
            error: null,
            refreshing:false,
            kitName:"",
            kitDescription:"",
            kitDate:"",
            xcounter: "",
            deleteProductId: "",
            checked: [],
            productIDx: []
        }
       
        navFrom = 'viewKit';
    }

    componentWillMount(){
        this._fetchKitInfo();
        this._fetchProduct();
    }

    //fetch kit info------------------------------------->
    _fetchKitInfo(){
        const  page  = this.state.page;
        const { navigation } = this.props;
        const apikey = 1;
        const token = tokenx;
        const access = accessx;
        const kitID = kitIDx;
        console.log(token);
        console.log(access);
        fetch('https://filterandindustrial.com.au/apix/Kits/info', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey:apikey,
                token: token,
                access: access,
                kitid: kitID
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson['code'] != 0 ){
                alert(responseJson['message']);
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                   this.props.navigation.push('SignInScreen');
                   return false;
                }
            }
            this.setState({
                kitName:responseJson.data['Kitname'],
                kitDescription: responseJson.data['description'],
                kitDate: responseJson.data['datex']
            });
        })
        .catch((error) => {
            console.error(error);
        })
    }

    //fetch kit product------------------------------->
    _fetchProduct(){
        const  page  = this.state.page;
        const { navigation } = this.props;
        const apikey = 1;
        const token = tokenx;
        const access = accessx;
        const limit = navigation.getParam('limit', 10);
        
        fetch('https://filterandindustrial.com.au/apix/Kits/ViewProduct', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: apikey,
                token: token,
                access: access,
                offset: page * 10,
                limit: limit,
                kitid: kitIDx
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
           if(responseJson['code'] != 0 ){
                alert(responseJson['message']);
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                   this.props.navigation.push('SignInScreen');
                   return false;
                }
            }
           this.setState({
              isLoading: false,
              dataSource: [...this.state.dataSource,...responseJson.data['items']],
              refreshing:false
            })

        })
        .catch((error) => {
          console.error(error);
        })
    }


    _deleteProduct(id){
        const apikey = 1;
        const token = tokenx;
        const access = accessx;
        fetch('https://filterandindustrial.com.au/apix/Kits/deleteProduct', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: apikey,
                token: token,
                access: access,
                kitId: kitIDx,
                kitproductID: id
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
             //check for errors
            if(responseJson['code'] != 0 ){
                alert(responseJson['message']);
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                   this.props.navigation.push('SignInScreen');
                   return false;
                }
            }
            this.setState({
                dataSource: [],
                page: 0
            })
            this._fetchProduct();
        })
        .catch((error) => {
          console.error(error);
        })
    }

    _handleLoadMoreProduct = () => {
        this.setState(
          (prevState, nextProps) => ({
            page: prevState.page + 1,
            loadingMore: true
          }),
          () => {
            this._fetchProduct();
          }
        )
    };

    ListEmptyView = () => {
        return (
            <View style={{flex: 1, paddingTop: 30}}>
                <Text style={{textAlign: 'center'}}>No Product to Display</Text>
                <View style={{paddingTop: 20, alingnItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
                    <Button style={{backgroundColor:'#FF0000', height: 40, borderRadius: 20 }} onPress={() => {this.props.navigation.navigate('AddProduct') }}>
                        <Text style={{color: '#fff'}}>ADD ITEM TO KIT</Text>
                    </Button>
                </View>
            </View>
        );
    };

    handleChange(index, e){
        let checked = [...this.state.checked];
       
        checked[index] = !checked[index];

        this.setState({ checked });

        if(!checked[index]==false){
            this.setState({ productIDx: [...this.state.productIDx, e] })
        }else{
            var array  = this.state.productIDx;

            var index = array.indexOf(e);
            if (index > -1) {
                array.splice(index, 1);
            }
        }
    };

    //convert to string 
    _converter(obj, separator) {
            var arr = [];

            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    arr.push(obj[key]);
                }
            }

            return arr.join(separator || ",");
    }
    
    

    _getProduct = () =>{
        let productRequest =   this._converter(this.state.productIDx,"|");
        let msgConfirm = '';
        if(productRequest==""){
            if(this.state.dataSource.length>0 ){
                msgConfirm = "You are ordering all the product of this kit. Continue sending order?";
            }else{
                msgConfirm = 'Please add Product in your Kit';
                alert(msgConfirm);
                return false;
            }
        }else{
            msgConfirm = 'You are ordering specific product on the kit. Continue sending order?'
        }
        

        Alert.alert(
            'Confirmation!', 
            msgConfirm,
            [
                {
                    text: 'Cancel', 
                    onPress: () => console.log('Cancel Pressed'), 
                    style: 'cancel', 
                },
                {
                    text: 'Yes', 
                    onPress: () => this._sendOrder()
                },
                {
                    cancelable: false
                },
            ]
        )
    }

    _sendOrder = () => {
        let productRequest =   this._converter(this.state.productIDx,"|");
        
        const apikey = 1;
        const token = tokenx;
        const access = accessx;
        fetch('https://filterandindustrial.com.au/apix/Kits/order', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: apikey,
                token: token,
                access: access,
                kitid: kitIDx,
                skuList: productRequest
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            //check errors
            if(responseJson['code'] != 0 ){
                alert(responseJson['message']);
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){ 
                    this.props.navigation.push('SignInScreen');
                }
            }else{
                alert('Order has been sent.');
            }
        })
        .catch((error) => {
              console.error(error);
        })

    }

    render() {
        let { data, checked } = this.state;
        if(this.state.isLoading){
            return(
                <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                    <Spinner color='#FF0000' />
                </View>
            )
        }
        return (
            <Container style={styles.container}>
                <Header transparent style={styles.header}>
                    <Left>
                        <Button transparent iconLeft style={{width: 200, height: 40}} 
                            onPress={() => { this.props.navigation.push('Dashboard') }}>
                            <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                                <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>VIEW KIT</Title>
                            </View>
                        </Button> 
                    </Left>        
                    <Right>

                    </Right>
                </Header>
                <Content>
                    <View style={{paddingHorizontal: 10, paddingTop: 50, paddingBottom: 15}}>
                        <View style={styles.titleContainer}>
                            <Icon type='FontAwesome'name="circle" style={styles.circleIcon}/>
                            <Text style={styles.text}>KITS</Text>
                        </View>
                        <View>
                            <Card style={styles.cardContainer}>                     
                                <CardItem header style={{paddingBottom: 0, backgroundColor: 'transparent'}}>
                                    <Row style={{zIndex: 0}}>
                                        <Col>
                                            <Row>
                                                <View style={{paddingRight: 5}}> 
                                                    <Badge style={styles.badgeCircle}>
                                                        <Text style={styles.textCircle}>{kitIDx}</Text>
                                                    </Badge>
                                                </View>
                                                <Col style={{paddingRight: 25}}>
                                                    <Text style={styles.titleText}>{this.state.kitName}</Text>
                                                    <Text style={styles.dateText}>{this.state.kitDate}</Text>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </CardItem>
                                <CardItem style={{paddingTop: 10, backgroundColor: 'transparent'}}>
                                    <Body>
                                        <Text style={{color: '#828080', fontSize: 14}}>{this.state.kitDescription}</Text>
                                    </Body>
                                </CardItem>
                                <View style={{position: 'absolute', right: 10,top: 3, backgroundColor: 'transparent', width: 99}}>
                                   <KitDropDownMenu/>
                                </View>
                                <View>
                                    <Row>
                                        <Col>
                                            <Button style={{backgroundColor: '#FF0000', height: 40, borderRadius: 20, alignSelf: 'center', width:130}} onPress={() => {this.props.navigation.navigate('AddProduct') }}>
                                                <Text style={{flex: 1, color: '#fff', textAlign: 'center'}}>ADD ITEM</Text>
                                            </Button>    
                                        </Col>
                                        <Col>
                                             <Button style={{backgroundColor: '#FF0000', height: 40, borderRadius: 20, alignSelf: 'center', width: 130}} onPress = {this._getProduct}>
                                                <Text style={{flex: 1, color: '#fff', textAlign: 'center'}}>ORDER</Text>
                                            </Button>    
                                        </Col>
                                    </Row>
                                </View>
                            </Card>
                        </View>
                    </View>
                    <View style={{paddingHorizontal: 10, paddingBottom: 10}}>
                        <Row>
                            <Col>
                                <Button full style={{backgroundColor:'#FF0000'}}>
                                   <Text style={{color: '#fff'}}>Product</Text>
                                </Button>
                            </Col>
                            <Col>
                                <Button full style={{backgroundColor:'#fff'}} onPress={()=>{this.props.navigation.push('RequestKit')}}>
                                   <Text style={{color: '#000'}}>Item Request</Text>
                                </Button>
                            </Col>
                        </Row> 
                    </View>
                    <FlatList
                        //numColumns={2}
                        data={this.state.dataSource}
                        extraData={this.state} 
                        renderItem={
                            ({item, index}) =>
                            <View style={{paddingBottom: 15, paddingHorizontal: 10}}>
                                <Card style={styles.cardProductContainer}>
                                    <CheckBox 
                                        iconleft 
                                        checkedColor='#FF0000'
                                        uncheckedColor = '#FF0000'
                                        containerStyle={{backgroundColor: 'transparent', width: 20, padding: 0, position: 'absolute', right: 0, zIndex: 99}} 
                                        onPress={() => 
                                            this.handleChange(index, item.prodNr)
                                        } 
                                        checked={
                                            checked[index]
                                        } 
                                    />
                                    <Row>
                                        <View style={{paddingRight: 10, justifyContent: 'center', alignItems: 'center'}}>
                                            <Image 
                                                style={{width: 100, height: 100}} 
                                                source={(item.ImageUrl != '') ? {uri: item.ImageUrl} : {uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}} 
                                            />
                                        </View>
                                        <Col>
                                            <Text style={{fontSize: 10, fontWeight: 'bold', textTransform: 'uppercase'}}>{item.prodNr}</Text>
                                            <Text style={{fontSize: 12, fontWeight: 'bold', textTransform: 'uppercase', color:'#707070', paddingTop: 10 }}>{item.title}</Text>
                                            <Text style={{fontSize: 10, fontWeight: 'bold', textTransform: 'uppercase', paddingTop: 10 }}>FILTER TYPE:</Text>
                                            <Text style={{fontSize: 10, textTransform: 'uppercase' }}>{item.filtertype}</Text>
                                            <Text style={{paddingTop: 10}}>
                                                <Text style={{fontSize: 10, fontWeight: 'bold'}}>QTY: </Text>
                                                <Text style={{fontSize: 10}}>{item.f_qty}</Text>
                                            </Text>
                                        </Col>
                                    </Row>
                                    <View style={{position: 'absolute', right: 10, height: 20, bottom: 10, backgroundColor: 'transparent', width: 100}}>
                                        <Row>
                                            <Col style={{paddingRight: 5}}>
                                                <Button 
                                                    style={{height: 20, backgroundColor: '#FF0000'}} 
                                                    onPress={() => {
                                                        this.props.navigation.push(
                                                            'EditProduct', 
                                                            {
                                                                productNr:item.prodNr, 
                                                                productQty: item.f_qty, 
                                                                productFilter: item.filtertype, 
                                                                productTitle: item.title, 
                                                                productId: item.kitProductID
                                                            }
                                                        );
                                                        global.kitproductIDxx = item.kitProductID;
                                                    }}>
                                                    <Icon type="FontAwesome" name="edit" style={{color: '#fff', fontSize: 12}}/>
                                                </Button>
                                            </Col>
                                            <Col style={{paddingLeft: 5}}>
                                                <Button style={{height: 20, backgroundColor: '#FF0000'}} 
                                                    onPress={()=>{ 
                                                        Alert.alert(
                                                            'Confirm Deletion', 
                                                            'Are you sure to delete this item to this kit?' ,
                                                            [
                                                                {
                                                                    text: 'Cancel', 
                                                                    onPress: () => console.log('Cancel Pressed'), 
                                                                    style: 'cancel', 
                                                                },
                                                                {
                                                                    text: 'Yes', 
                                                                    onPress: () => this._deleteProduct(item.kitProductID)
                                                                },
                                                                {
                                                                    cancelable: false
                                                                },
                                                            ]
                                                        )
                                                    }}
                                                >
                                                    <Icon type="FontAwesome" name="trash" style={{color: '#fff', fontSize: 12}}/>
                                                </Button>
                                            </Col>
                                        </Row>
                                    </View>
                                </Card>
                            </View>
                        }
                        keyExtractor={item => item.kitProductID}
                        onEndReached={this._handleLoadMoreProduct}
                        onEndReachedThreshold={1}
                        initialNumToRender={0}
                        refreshing={this.state.refreshing}
                        ListEmptyComponent={this.ListEmptyView}
                    />
                </Content>
            </Container>
        );
    }
}
export default withNavigation(ViewKitContent);

const styles = StyleSheet.create({
  header:{
      height: 40  
  },
  button:{
    backgroundColor: '#FF0000',
    borderRadius: 10,
    height: 30,
  },
  iconLeft:{
    color: '#FF0000', 
    fontSize: 16, 
    marginLeft: 0,
    ...Platform.select({
      android:{
        marginLeft: -10,
      }
    })
  },

  container:{
    backgroundColor: 'transparent',
    borderColor: 'transparent',
  },

  titleContainer:{
    flexDirection: 'row',
    textAlignVertical: 'center'

  },
  circleIcon:{
    color: '#FF0000',
    paddingRight: 10,
    paddingTop: 5,
    fontSize: 14
  },
  text:{
    fontWeight: 'bold'
  },

  cardContainer:{
    borderRadius: 10,
    minHeight: 150,
  },
  cardProductContainer:{
    minHeight: 100,
    borderRadius: 0,
    padding: 10
  },
  circleIcon:{
    color: '#FF0000',
    paddingRight: 10,
    paddingTop: 5,
    fontSize: 14
  },
  button:{
    backgroundColor: '#fff',
    borderRadius: 20
  },
  iconLight:{
    fontWeight: '100'
  },
  text:{
    fontWeight: 'bold'
  },
  badgeCircle:{
    height: 30,
    width: 30,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textCircle:{
    fontSize: 8,
    fontWeight: 'bold',
    width: 30, 
  },
  titleText:{
     fontSize: 12
  },
  dateText:{
      fontSize: 9,
      color: '#828080'
  }
});