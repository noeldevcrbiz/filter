import React, { Component } from 'react';
import { StyleSheet,ImageBackground} from 'react-native';
import { Container} from 'native-base';
import ViewKitContent from './ViewKitContent';

export default class ViewKit extends Component {
  render() {
    return (
        <ImageBackground source={require('./../../assets/img/bg.png')} style={styles.imgWrapper}>
            <Container style={styles.container}>
                <ViewKitContent/>
            </Container>
        </ImageBackground>
    );
  }
}


const styles = StyleSheet.create({
    imgWrapper: {
        flex: 1
    },
    container:{
        backgroundColor: 'transparent'
    }
});