import React, { Component } from 'react';
import {Platform, StyleSheet, View, FlatList, Image, Separator, TouchableOpacity, Alert, TextInput} from 'react-native';
import {Header, Left, Right, Button, Icon, Title, Container, Content, Text, Card, CardItem, Row, Col, Badge, Body, Spinner, H1, Item, List, ListItem } from 'native-base';
//import ActionSheet from 'react-native-actionsheet';
import Modal from "react-native-modal";
import { withNavigation } from 'react-navigation';

import KitDropDownMenu from './KitDropDown';


class ViewKitContentRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true, 
            dataSourcexx: [],
            page:0,
            loadingMore: false,
            loading: true,
            error: null,
            refreshing:false,
            kitName:"",
            kitDescription:"",
            kitDate:"",
            xcounter: "",
            deleteProductId: "",
            addRequest: "",
            isModalVisible: false
        }
       
        navFrom = 'viewKit';
        
    }

    componentWillMount(){
       this._fetchKitInfo();
       this._displayRequestList();
    }

    //fetch kit info------------------------------------->
    _fetchKitInfo(){
        const  page  = this.state.page;
        const { navigation } = this.props;
        const apikey = 1;
        const token = tokenx;
        const access = accessx;
        const kitID = kitIDx;

        fetch('https://filterandindustrial.com.au/apix/Kits/info', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey:apikey,
                token: token,
                access: access,
                kitid: kitID
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson['code'] != 0 ){
                alert(responseJson['message']);
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                    this.props.navigation.push('SignInScreen');
                }
            }
            this.setState({
                isLoading: false,
                kitName:responseJson.data['Kitname'],
                kitDescription: responseJson.data['description'],
                kitDate: responseJson.data['datex']
            });
        })
        .catch((error) => {
            console.error(error);
        })
    };


    //Add Request Product ------------------------------------>
    addRequestProduct = () => {

        const apikey = 1;
        const token = tokenx;
        const access = accessx;
        const prodReq = this.state.addRequest;

        if (prodReq == ''){
            alert('Please add title on requested product');
            return false;
        }

        fetch('https://filterandindustrial.com.au/apix/Kits/updaterequest', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: apikey,
                token: token,
                access: access,
                kitid: kitIDx,
                prodReq: prodReq
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({ isModalVisible: !this.state.isModalVisible });
            if(responseJson['code'] != 0 ){
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                    alert(responseJson['message']);
                    this.props.navigation.push('SignInScreen');
                }
            }
            this.props.navigation.push('RequestKit');
        })
        .catch((error) => {
          console.error(error);
        })
    }

    //Display Request ------------------------------------>
    _displayRequestList(){
        const apikey = 1;
        const token = tokenx;
        const access = accessx;
        fetch('https://filterandindustrial.com.au/apix/Kits/Viewrequest', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: apikey,
                token: token,
                access: access,
                kitid: kitIDx
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson['code'] != 0 ){
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                    alert(responseJson['message']);
                    this.props.navigation.push('SignInScreen');
                }
            }
           this.setState({
              isLoading: false,
              dataSourcexx: [...this.state.dataSourcexx,...responseJson.data['items']],
              refreshing:false
            })
        })
        .catch((error) => {
          console.error(error);
        })
    }

    //Delete Request ------------------------------------>
    _deleteRequest = (x) => {
        const apikey = 1;
        const token =  tokenx;
        const access = accessx;
        const prodReq = x;

        fetch('https://filterandindustrial.com.au/apix/Kits/Deleterequest', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ apikey:apikey,
                apikey: apikey,
                token: token,
                access: access,
                kitid: kitIDx,
                prodReq: prodReq
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson['code'] != 0 ){
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                    alert(responseJson['message']);
                    this.props.navigation.push('SignInScreen');
                }
            }
            this.props.navigation.push('RequestKit');
        })
        .catch((error) => {
            console.error(error);
        })
    }

    //if list is empty
    ListEmptyView = () => {
        return (
            <View style={{flex: 1}}>
                <Text style={{textAlign: 'center'}}>No Product to Display</Text>
            </View>
        );
    };

    FlatListItemSeparator = () => {
        return (
          <View style={{height: 1}}></View>
        )
    };

    //send request
    sendRequest = () =>{
        const apikey = 1;
        const token =  tokenx;
        const access = accessx;

        fetch('https://filterandindustrial.com.au/apix/Kits/sendproductrequest', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ apikey:apikey,
                apikey: apikey,
                token: token,
                access: access,
                kitid: kitIDx
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson['code'] != 0 ){
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                    alert(responseJson['message']);
                    this.props.navigation.push('SignInScreen');
                 //check if there is an item to send
                }else if(responseJson['code'] == 21) {
                    Alert.alert(
                        'Alert',
                        'No item to send! Please add an item before sending a request.',
                        [
                            {
                                text: 'Cancel', 
                                onPress: () => console.log('Cancel Pressed'), 
                                style: 'cancel', 
                            },
                            {
                                text: 'Add Item', 
                                onPress: () => this.setState({ isModalVisible: !this.state.isModalVisible })
                            },
                            
                            {
                                cancelable: false
                            },
                        ]
                    )
                }
            }else{
                alert('Request item sent!');
            }
        })
        .catch((error) => {
            console.error(error);
        })

    }


    //modal toggle
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    render() {
        if(this.state.isLoading){
            return(
                <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                    <Spinner color='red' />
                </View>
            )
        }
        return (
            <Container style={styles.container}>
                <Header transparent style={styles.header}>
                    <Left>
                        <Button transparent iconLeft style={{width: 200, height: 40}} 
                            onPress={() => { this.props.navigation.push('Dashboard') }}>
                            <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                                <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>VIEW KIT</Title>
                            </View>
                        </Button> 
                    </Left>        
                    <Right>

                    </Right>
                </Header>
                <Content>
                    <View style={{paddingHorizontal: 10, paddingTop: 50, paddingBottom: 15}}>
                        <View style={styles.titleContainer}>
                            <Icon type='FontAwesome'name="circle" style={styles.circleIcon}/>
                            <Text style={styles.text}>KITS</Text>
                        </View>
                        <View>
                            <Card style={styles.cardContainer}>                     
                            <CardItem header style={{paddingBottom: 0, backgroundColor: 'transparent'}}>
                                <Row style={{zIndex: 0}}>
                                    <Col>
                                        <Row>
                                            <View style={{paddingRight: 5}}> 
                                                <Badge style={styles.badgeCircle}>
                                                    <Text style={styles.textCircle}>{kitIDx}</Text>
                                                </Badge>
                                            </View>
                                            <Col style={{paddingRight: 25}}>
                                                <Text style={styles.titleText}>{this.state.kitName}</Text>
                                                <Text style={styles.dateText}>{this.state.kitDate}</Text>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                </CardItem>
                                <CardItem style={{paddingTop: 10, backgroundColor: 'transparent'}}>
                                    <Body>
                                        <Text style={{color: '#828080', fontSize: 14}}>{this.state.kitDescription}</Text>
                                    </Body>
                                </CardItem>
                                <View style={{position: 'absolute', right: 10, top: 3, backgroundColor: 'transparent', width: 99}}>
                                    <KitDropDownMenu/>
                                </View>
                                 <View>
                                <Row>
                                    <Col>
                                        <Button style={{backgroundColor: '#FF0000', height: 40, borderRadius: 20, alignSelf: 'center', width:130}} onPress={() => {this.props.navigation.navigate('AddProduct') }}>
                                            <Text style={{flex: 1, color: '#fff', textAlign: 'center'}}>ADD ITEM</Text>
                                        </Button>    
                                    </Col>
                                    <Col> 
                                    </Col>
                                </Row>
                            </View>
                            </Card>
                        </View>
                    </View>
                    <View style={{paddingHorizontal: 10, paddingBottom: 10}}>
                        <Row>
                            <Col>
                                <Button full style={{backgroundColor:'#fff'}} onPress={()=>{this.props.navigation.push('ViewKit')}}>
                                   <Text style={{color: '#000'}}>Product</Text>
                                </Button>
                            </Col>
                            <Col>
                                 <Button full style={{backgroundColor:'#FF0000'}}>
                                   <Text style={{color: '#fff'}}>Item Request</Text>
                                </Button>
                            </Col>
                        </Row> 
                    </View>
                    <View style={{paddingHorizontal: 10, paddingBottom: 10}}>
                        <Row>
                            <Col style={{paddingRight: 5}}>
                               <Button full style={{height: 35, backgroundColor: '#fff', borderRadius: 15}} onPress={this.toggleModal}>
                                    <Row>
                                        <Col style={{justifyContent: 'center'}}>
                                            <Text style={{textAlign: 'left', color: '#000', paddingLeft: 15, fontSize: 11}}>ITEM REQUEST</Text>
                                        </Col>
                                        <View style={{alignItems:'flex-end', justifyContent: 'center' }}>
                                            <Icon type="FontAwesome" name='shopping-bag' style={{color: '#FF0000', fontSize: 16 }}/>
                                        </View>
                                    </Row>
                                </Button> 
                            </Col>
                            <Col style={{paddingLeft: 5}}>
                                <Button full style={{height: 35, backgroundColor: '#fff', borderRadius: 15}}
                                    onPress={()=>
                                        { 
                                            Alert.alert(
                                                'Confirmation',
                                                'Send request item?',
                                                [
                                                    {
                                                        text: 'Add More Item', 
                                                        onPress: () => {this.setState({ isModalVisible: !this.state.isModalVisible })}
                                                    },
                                                    {
                                                        text: 'Send', 
                                                        onPress: () => {this.sendRequest()}
                                                    },
                                                    {
                                                        text: 'Cancel', 
                                                        onPress: () => console.log('Cancel Pressed'), 
                                                        style: 'cancel', 
                                                    },
                                                    {
                                                        cancelable: false
                                                    },
                                                ]
                                            )
                                        }
                                    }
                                >
                                    <Row>
                                        <Col style={{justifyContent: 'center'}}>
                                            <Text style={{textAlign: 'left', color: '#000', paddingLeft: 15, fontSize: 11}}>SEND REQUEST</Text>
                                        </Col>
                                        <View style={{alignItems:'flex-end', justifyContent: 'center' }}>
                                            <Icon type="FontAwesome" name='envelope' style={{color: '#FF0000', fontSize: 16 }}/>
                                        </View>
                                    </Row>
                                </Button> 
                            </Col>
                        </Row>
                    </View>
                    <View>
                        <Modal 
                            isVisible={this.state.isModalVisible}
                        >
                            <View style={{ flex: 1, justifyContent: 'center', alingItems: 'center'}}>
                                <Row style={{height: 40}}>
                                    <Col>
                                        <Item rounded style={styles.inputContainer}>
                                            <TextInput placeholderTextColor="#C1C1C1" 
                                                placeholder="Add Request Item"
                                                style={styles.input} 
                                                onChangeText={(addRequest) => this.setState({addRequest})} 
                                                value={this.state.addRequest} 
                                            />
                                        </Item>
                                    </Col>
                                    <Button 
                                        style={{borderRadius: 0, height: 40, backgroundColor: '#FF0000'}}
                                        onPress={this.addRequestProduct}>
                                       <Text style={{fontSize: 14, color: '#fff'}}>ADD</Text>
                                    </Button>
                                </Row>
                                <Button style={{marginTop: 15, backgroundColor: '#FF0000', alignSelf: 'center'}} onPress={this.toggleModal}><Text>Cancel</Text></Button>
                            </View>
                        </Modal>
                    </View>
                     <FlatList
                        data={this.state.dataSourcexx}
                        ItemSeparatorComponent = {this.FlatListItemSeparator} 
                        renderItem = {
                            ({item, index}) =>
                                <View style={{paddingHorizontal: 10, paddingBottom: 10}}>
                                    <Row style={{alignItems: 'center'}}>
                                        <Col style={{paddingRight: 10}}>
                                            <Text style={{alignSelf:'stretch'}}>{item.ctr}. {item.value}</Text>
                                        </Col>
                                        <View>
                                            <Button transparent
                                                onPress={()=>
                                                    { 
                                                        Alert.alert(
                                                            'Confirm Deletion', 
                                                            'Are you sure to delete this item request?' ,
                                                            [
                                                                {
                                                                    text: 'Cancel', 
                                                                    onPress: () => console.log('Cancel Pressed'), 
                                                                    style: 'cancel', 
                                                                },
                                                                {
                                                                    text: 'Yes', 
                                                                    onPress: () => this._deleteRequest(item.value)
                                                                },
                                                                {
                                                                    cancelable: false
                                                                },
                                                            ]
                                                        )
                                                    }
                                                }
                                            >
                                                <Icon type='FontAwesome' style={{color: '#FF0000'}} name='trash'/>
                                            </Button>
                                        </View>
                                    </Row>
                                </View>
                        }
                        keyExtractor={(item, index) => index.toString()}
                        initialNumToRender={1}
                        ListEmptyComponent={this.ListEmptyView}
                    />
                </Content>
            </Container>
        );
    }
}
export default withNavigation(ViewKitContentRequest);

const styles = StyleSheet.create({
  header:{
      height: 40  
  },
  button:{
    backgroundColor: '#FF0000',
    borderRadius: 10,
    height: 30,
  },
  iconLeft:{
    color: '#FF0000', 
    fontSize: 16, 
    marginLeft: 0,
    ...Platform.select({
      android:{
        marginLeft: -10,
      }
    })
  },

  inputContainer:{
    borderRadius: 0,
    height: 40,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 0,  
    //elevation: 5,
    //marginHorizontal: 10
  },

  input:{
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000',
    paddingHorizontal: 15,
    flex: 1,
    flexDirection: 'row'
  },

  container:{
    backgroundColor: 'transparent',
    borderColor: 'transparent',
  },

  titleContainer:{
    flexDirection: 'row',
    textAlignVertical: 'center'

  },
  circleIcon:{
    color: '#FF0000',
    paddingRight: 10,
    paddingTop: 5,
    fontSize: 14
  },
  text:{
    fontWeight: 'bold'
  },

  cardContainer:{
    borderRadius: 10,
    minHeight: 150,
  },
  cardProductContainer:{
    minHeight: 100,
    borderRadius: 0,
    padding: 10
  },
  circleIcon:{
    color: '#FF0000',
    paddingRight: 10,
    paddingTop: 5,
    fontSize: 14
  },
  button:{
    backgroundColor: '#fff',
    borderRadius: 20
  },
  iconLight:{
    fontWeight: '100'
  },
  text:{
    fontWeight: 'bold'
  },
  badgeCircle:{
    height: 30,
    width: 30,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textCircle:{
    fontSize: 8,
    fontWeight: 'bold',
    width: 30, 
  },
  titleText:{
     fontSize: 12
  },
  dateText:{
      fontSize: 9,
      color: '#828080'
  }
});