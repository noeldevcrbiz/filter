import React, { Component } from 'react';
import { StyleSheet,ImageBackground} from 'react-native';
import { Container} from 'native-base';
import ViewKitContentRequest from './ViewKitContentRequest';

export default class RequestKit extends Component {
  render() {
    return (
        <ImageBackground source={require('./../../assets/img/bg.png')} style={styles.imgWrapper}>
            <Container style={styles.container}>
                <ViewKitContentRequest/>
            </Container>
        </ImageBackground>
    );
  }
}


const styles = StyleSheet.create({
    imgWrapper: {
        flex: 1
    },
    container:{
        backgroundColor: 'transparent'
    }
});