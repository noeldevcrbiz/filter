import React, {Component} from 'react';
import { Icon, Text, View, Row, Col, Button } from 'native-base';
import { withNavigation } from 'react-navigation';

class DropdownMenu extends Component {
    render() {
        return (
			<Row>
				<Col style={{paddingRight: 5}}>
					<Button style={{height: 20, backgroundColor: '#FF0000'}} onPress={() => { this.props.navigation.navigate('EditKit') }}>
						<Icon type='FontAwesome' name='edit' style={{fontSize: 12, color: '#fff'}} />
					</Button>
				</Col>
				<Col style={{paddingLeft: 5}}>
					<Button style={{height: 20, backgroundColor: '#FF0000'}} onPress={() => {this.props.navigation.navigate('CopyKit')}}>
						<Icon type='FontAwesome' name='copy' style={{fontSize: 12, color: '#fff'}} />
					</Button>
				</Col>
			</Row>
        );
    }
  }
export default withNavigation(DropdownMenu);

