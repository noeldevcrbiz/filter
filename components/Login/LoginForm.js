import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, StatusBar} from 'react-native';

class LoginForm extends Component {
  handleClick = () => {
     return fetch('https://filterandindustrial.com.au/apix/member/Login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        apikey: 1,
        username: 'jeffreytanglao@gmail.com',
        password: '123456',
      }),
    })
    .then((response) => response.json())
    .then((responseJson) => {
         //check for errors
        if(responseJson['code'] != 0 ){
            alert(responseJson['message']);
            if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                this.props.navigation.push('SignInScreen');
                return false;
            }
        }
        if(responseJson.code=="0"){
          this.props.navigation.push('Dashboard')
        }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {

    return (
        <View style={styles.container}>
          <StatusBar
            backgroundColor="#fff"
            barStyle="light-content"
          />
          <TextInput placeholder="Username or Email" 
            style={styles.input}
            returnKeyType="next"
            onSubmitEditing={()=>this.passwordInput.focus()}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
          />
          <TextInput placeholder="Password" 
            style={styles.input}
            returnKeyType="next"
            secureTextEntry
            ref={(input)=>this.passwordInput = input}
          />

          <TouchableOpacity style={styles.buttonContainer} onPress={this.handleClick}>
            <Text style={styles.buttonText}>LOGIN</Text>
          </TouchableOpacity>
        </View>

    );
  }
}

export default LoginForm;

const styles = StyleSheet.create({
  container:{

    paddingTop: 15,
    alignItems: 'center'
  },
  input:{
    width: 250,
    height: 35,
    backgroundColor: '#fff',
    marginBottom: 20,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  buttonContainer:{
     backgroundColor: '#FF0000',
     width: 100,
     borderRadius: 20
  },
  buttonText:{
    textAlign: 'center',
    color: '#fff',
    paddingVertical: 10,
  }
});
