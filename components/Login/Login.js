import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar, ImageBackground, KeyboardAvoidingView} from 'react-native';
import LoginForm from './LoginForm';

export default class Login extends Component {
  render() {
    const version = "1.0.0";
    return (
      <ImageBackground source={require('./../../assets/img/bg.png')} style={styles.imgWrapper}>
        <KeyboardAvoidingView behavior="padding" style={styles.container}>
          <View style={styles.contentWrapper}>
            <Text style={styles.textTitle}>Filter and Industrial</Text>
            <Text style={{fontSize: 12}}>Version: {version}</Text>
            <LoginForm/>
          </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
  },
  imgWrapper: {
    flex: 1,
  },
  contentWrapper:{
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
    
  },
  textTitle:{
    fontSize: 35,
    fontWeight: 'bold'

  },

});
