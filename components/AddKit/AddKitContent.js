import React, { Component } from 'react';
import {Platform, StyleSheet,TextInput} from 'react-native';
import {Header, Left, Right, Button, Icon, Title, Text, Container, Item, Input, View, Form} from 'native-base';
import { withNavigation } from 'react-navigation';

class AddKitContent extends React.Component {

  handleClick = () => {
    const kitTitle = this._kitTitle._lastNativeText;
    const kitDescription = this._kitDescription._lastNativeText;

    if(!kitTitle){
      alert('Please add kit title');
      return false;
    }
    if(!kitDescription){
      alert('Please add kit description');
      return false;
    }

    return fetch('https://filterandindustrial.com.au/apix/Kits/add', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        apikey: 1,
        token: tokenx,
        access: accessx,
        name: this._kitTitle._lastNativeText,
        short_description: this._kitDescription._lastNativeText,
      }),
    })
    .then((response) => response.json())
    .then((responseJson) => {

      //check for errors
      if(responseJson['code'] != 0 ){
        if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
          alert(responseJson['message']);
          this.props.navigation.push('SignInScreen');
          return false;
        }
      }

      if(responseJson['code']=="0"){
        global.kitIDx = responseJson.data['kitID'];
        this.props.navigation.push('ViewKit');
      }else{
        alert('Error!');
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {
     return (
        <Container style={styles.container}>
          <Header transparent style={styles.header}>
            <Left>
              <Button transparent iconLeft style={{width: 200, height: 40}} onPress={() => { this.props.navigation.push('Dashboard') }}>
                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                  <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                  <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>ADD KIT</Title>
                </View>
              </Button>
            </Left>
            <Right>
              <Button iconRight style={styles.button} onPress={this._addKitAsync} >
                <Text style={styles.text}>NEXT</Text>
                <Icon type='FontAwesome' name='chevron-right' style={styles.icon}/>
              </Button>
            </Right>
          </Header>
          <View style={{paddingHorizontal: 10, paddingTop: 20}}>
            <View style={{paddingBottom: 20}}>
              <Text style={{fontSize: 12}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim leo, fermentum in lacinia nec, iaculis eget libero</Text>
            </View>
            <Form>
              <Item rounded style={styles.inputContainer}>
                <TextInput placeholderTextColor="#C1C1C1" 
                  placeholder='KIT TITLE' 
                  style={styles.input} 
                  ref={_kitTitle => this._kitTitle = _kitTitle} 
                />
              </Item>
              <View style={styles.textAreaContainer}>
                <TextInput bordered placeholderTextColor="#C1C1C1" placeholder="KIT SHORT DESCRIPTION" multiline={true} numberOfLines={10} style={styles.textArea} 
                  ref={_kitDescription => this._kitDescription = _kitDescription} 
                />
              </View>
            </Form>
          </View>
      </Container>
    );
  }

  _addKitAsync = async () => {
    responsex = this.handleClick();
  };
}

export default withNavigation(AddKitContent);


const styles = StyleSheet.create({
    header:{
        height: 40  
    },
    button:{
      backgroundColor: '#FF0000',
      borderRadius: 10,
      height: 30,
    },
    iconLeft:{
      color: '#FF0000', 
      fontSize: 16, 
      marginLeft: 0,
      ...Platform.select({
        android:{
          marginLeft: -10,
        }
      })
    },
    text:{
      fontWeight: 'bold'
    },
    icon:{
      fontSize: 12,
      color: '#fff'
    },
     container:{
      backgroundColor: 'transparent',
      borderColor: 'transparent',
  },
  inputContainer:{
    borderRadius: 10,
    height: 30,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5
  },
  input:{
    fontSize: 12,
    fontWeight: 'bold',
    color: '#000',
    paddingHorizontal: 15, 
    flex: 1
  },
  textAreaContainer:{
    paddingTop: 30
  },
  textArea:{
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5,
    padding: 5,
    fontSize: 12,
    color: '#000',
    fontWeight: 'bold',
    height: 150,
    textAlignVertical: 'top'
  }
});