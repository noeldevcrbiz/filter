import React, { Component } from 'react';
import {Platform, StyleSheet, TextInput} from 'react-native';
import {Header, Left, Right, Button, Icon, Title, Container, Content, Text, Form, Item, View} from 'native-base';
import { withNavigation } from 'react-navigation';

class EditKitContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true, 
      kitName:"",
      kitDescription:""
    }
  }
  componentWillMount(){
    this.setstate = ({
        isLoading: true, 
    });
    this._editKit();
  }

  _editKit(){
    const apikey = 1;
    const token = tokenx;
    const access = accessx;

    fetch('https://filterandindustrial.com.au/apix/Kits/info', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          apikey:apikey,
          token: token,
          access: access,
          kitid: kitIDx
      }),
  })
  .then((response) => response.json())
  .then((responseJson) => {
    //check for errors
    if(responseJson['code'] != 0 ){
        alert(responseJson['message']);
        if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
            this.props.navigation.push('SignInScreen');
            return false;
        }
    }
    this.setState({
      kitName:responseJson.data['Kitname'],
      kitDescription: responseJson.data['description'],
    });
  })
  .catch((error) => {
    console.error(error);
    })
  }


  handleClick = () => {
    const kitTitle = this.state.kitName;
    const kitDescription = this.state.kitDescription;

    if(!kitTitle){
      alert('Please add kit title');
      return false;
    }
    if(!kitDescription){
      alert('Please add kit description');
      return false;
    }

    return fetch('https://filterandindustrial.com.au/apix/Kits/edit', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        apikey: 1,
        token: tokenx,
        access: accessx,
        kitid: kitIDx,
        name: kitTitle,
        short_description: kitDescription
      }),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      //check for errors
      if(responseJson['code'] != 0 ){
          alert(responseJson['message']);
          if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
              this.props.navigation.push('SignInScreen');
              return false;
          }
      }

      if(responseJson.code=="0"){
        if(navFrom == 'dashBoard'){
          this.props.navigation.push('Dashboard');
        }else{
          counterx = 0;
          this.props.navigation.push('ViewKit');
        }
      }else{
        alert('Error!');
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {
     return (
      <Container style={styles.container}>
        <Header transparent style={styles.header}>
          <Left>
            <Button transparent iconLeft style={{width: 200, height: 40}} 
              onPress={() => { 
                     if(navFrom == 'dashBoard'){
                      this.props.navigation.push('Dashboard');
                    }else{
                      this.props.navigation.push('ViewKit');
                    }
                  }
                }
              >
              <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>EDIT KIT</Title>
              </View>
            </Button> 
          </Left>        
          <Right>
            <Button iconRight style={styles.buttonRight} onPress={this._addKitAsync}>
              <Text style={styles.text}>UPDATE</Text>
              <Icon type='FontAwesome' name='chevron-right' style={styles.icon}/>
            </Button>
          </Right>
        </Header>
        <View style={{paddingHorizontal: 10, paddingTop: 50}}>
          <Form>
            <Item rounded style={styles.inputContainer}>
              <TextInput placeholderTextColor="#C1C1C1" 
                style={styles.input} onChangeText={(kitName) => this.setState({kitName})} 
                value={this.state.kitName} 
              />
            </Item>
            <View style={styles.textAreaContainer}>
              <TextInput bordered placeholderTextColor="#C1C1C1" 
                style={styles.textArea} onChangeText={(kitDescription) => this.setState({kitDescription})} 
                value={this.state.kitDescription} 
                multiline={true} 
                numberOfLines={10}   
              />
            </View>
          </Form>
        </View>
      </Container>
    );
  }
  _addKitAsync = async () => {
    responsex = this.handleClick();
  };
}

export default withNavigation(EditKitContent);


const styles = StyleSheet.create({
  header:{
      height: 40  
  },
  icon:{
    fontSize: 12,
    color: '#fff'
  },
  buttonRight:{
    backgroundColor: '#FF0000',
    borderRadius: 10,
    height: 30,
  },
  iconLeft:{
    color: '#FF0000', 
    fontSize: 16, 
    marginLeft: 0,
    ...Platform.select({
      android:{
        marginLeft: -10,
      }
    })
  },

  inputContainer:{
    borderRadius: 10,
    height: 30,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5
  },
  textAreaContainer:{
    paddingTop: 30
  },
  input:{
    fontSize: 12,
    fontWeight: 'bold',
    color: '#000',
    paddingHorizontal: 15,
    flex: 1,
    flexDirection: 'row'
  },
  textArea:{
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5,
    padding: 5,
    fontSize: 12,
    color: '#000',
    fontWeight: 'bold',
    height: 150,
    textAlignVertical: 'top'
  },

  container:{
    backgroundColor: 'transparent',
    borderColor: 'transparent',
  },
  circleIcon:{
    color: '#FF0000',
    paddingRight: 10,
    paddingTop: 5,
    fontSize: 14
  },
  button:{
    backgroundColor: '#fff',
    borderRadius: 20
  },
  text:{
    fontWeight: 'bold'
  }
});