import React, { Component } from 'react';
import { StyleSheet,ImageBackground, Image, FlatList, List, ListItem, Separator, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Container, Content, Header, Left, Body, Right, Button, Icon, Title, Text, Input, View, Form, Textarea, Item, Card, CardItem,  Badge, Row, Col } from 'native-base';
import { SearchBar } from 'react-native-elements';
import { withNavigation } from 'react-navigation';


class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            loadingProps: {}, 
            dataSource: [],
            page:0,
            loadingMore: false,
            loading: true,
            error: null,
            refreshing:false,
            searchx: ''
        }
        
    };

    updateSearch = () => {
        const search = this.state.searchx;
        this._searching(encodeURIComponent(search));
        this.setState({ search});
    };

    _searching = (q, key, item) =>{
        rootDom = 'http://filternew.devcrbiz.com';
        this.setState({
          isLoading: false,
          dataSource: [],
          refreshing:false
        })

        fetch(
            'https://filterandindustrial.com.au/ajaxprocess/?query='+q+'&action=searchanythingbomapi', {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
           //check for errors
          if(responseJson['code'] != 0 ){
              if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                  alert(responseJson['message']);
                  this.props.navigation.push('SignInScreen');
                  return false;
              }
          }
          this.setState({
              isLoading: false,
              dataSource: [...this.state.dataSource,...responseJson],
              refreshing:false
            });
        })
        .catch((error) => {
            console.error(error);
        })
    };

    FlatListItemSeparator = () => {
        return (
          <View style={{height: 1}}></View>
        )
    };

    ListEmptyView = () => {
        return (
            <View style={{flex: 1}}>
                <Text style={{textAlign: 'center'}}>No Product to Display</Text>
            </View>
        );
    };


    render() {
        return (
            <ImageBackground source={require('./../../assets/img/bg.png')} style={styles.imgWrapper}>
                <Container style={styles.container}>
                    <Header style={styles.header}>
                        <Left>
                            <Button transparent style={{width: 200}} onPress={() => { this.props.navigation.goBack()}}>
                                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                    <Icon type="FontAwesome" name='chevron-left' style={{color: '#FF0000', fontSize: 16}}/>
                                    <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>SEARCH PRODUCT</Title>
                                </View>
                            </Button>
                        </Left>
                        <Right>
                        </Right>
                    </Header>
                    <Container style={styles.containerSearch}>
                        <View style={{paddingBottom: 20, paddingHorizontal: 5}}>
                            <Text style={{fontSize: 12, fontWeight: '100'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim leo, fermentum in lacinia nec, iaculis eget libero</Text>
                        </View>
                        <Row style={{ height: 42 }}>
                            <Col style={{ height: 42 }}>
                                <SearchBar onSplaceholder="Type Here..." onChangeText={(searchx)=>this.setState({searchx})} value={this.state.searchx} lightTheme containerStyle={{backgroundColor: 'transparent', padding: 0, borderTopColor: 'transparent', borderBottomColor: 'transparent'}} inputContainerStyle={{backgroundColor: '#fff', borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderTopRightRadius:0, borderBottomRightRadius: 0,shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8,shadowRadius: 2, elevation: 5}}/>
                            </Col>
                            <Button style={{width: 50, height: 42, backgroundColor: '#FF0000', borderTopLeftRadius: 0, borderBottomLeftRadius: 0, borderTopRightRadius:5, borderBottomRightRadius: 5}} onPress={() => { this.updateSearch() }}>
                                <View style={{flex:1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                    <Icon type="FontAwesome" name='search' style={{color: '#fff', fontSize: 16}}/>
                                </View>
                            </Button>
                        </Row>
                        <Content style={{paddingTop: 20}}>
                            <FlatList
                                data={this.state.dataSource}
                                ItemSeparatorComponent = {this.FlatListItemSeparator} 
                                renderItem={
                                    ({item, index}) =>
                                    <View style={{paddingBottom: 20}}>
                                        <Card style={{borderRadius: 10}}>                     
                                            <CardItem style={{borderRadius: 10}}>
                                               <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{paddingRight: 0}}>
                                                       <Image
                                                              style={{width: 100, height: 100}}
                                                              source={(item.ImageUrl != '') ? {uri: item.ImageUrl} : {uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
                                                        />
                                                    </View>
                                                    <Col style={{paddingLeft: 10}}>
                                                        <View>
                                                            <Text style={styles.sku}>{item.prodNr}</Text>
                                                            <Text style={styles.productTitle}>{item.title}</Text>
                                                        </View>
                                                    </Col>
                                                </View>
                                            </CardItem>
                                            <View style={{position: 'absolute', top: 5, right: 15}}>
                                                <Content>
                                                    <Button small style={{borderRadius: 20, height: 25, backgroundColor: '#FF0000'}} 
                                                        onPress={()=>{this.props.navigation.push('AddProduct', {productTitle: item.title, productNr: item.prodNr})}}>
                                                        <Icon type="FontAwesome" name='plus' style={styles.icon}/>
                                                        <Text style={styles.fontSmall}>ADD</Text>
                                                    </Button>
                                                </Content>
                                            </View>
                                        </Card>
                                    </View>
                                }
                                keyExtractor={item => item.f_id}
                                initialNumToRender={1}
                                refreshing={this.state.refreshing}
                                ListEmptyComponent={this.ListEmptyView}
                            />
                        </Content>
                    </Container>
                </Container>
            </ImageBackground>
        );
    }
}

export default withNavigation(Search);

const styles = StyleSheet.create({
    header:{
        backgroundColor: 'transparent',
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        elevation:0        
    },
    button:{
      backgroundColor: '#FF0000',
      borderRadius: 10,
      height: 30,
    },

    imgWrapper: {
        flex: 1
    },
    container:{
        backgroundColor: 'transparent'
    },

    containerSearch:{
      backgroundColor: 'transparent',
      borderColor: 'transparent',
      paddingHorizontal: 10,
    },
    inputContainer:{
        borderRadius: 5,
        height: 30,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,  
        elevation: 5
    },
    input:{
        fontSize: 12,
        fontWeight: 'bold',
        color: '#747474'
    },
    input2:{
        fontSize: 12,
        //fontWeight: 'bold',
        color: '#747474'
    },
    textAreaContainer:{
        paddingTop: 30
    },

    sku:{
        fontSize: 10,
        lineHeight: 10,
        fontWeight: 'bold',
        paddingBottom: 10,
        color: '#FF0000'
    },
    productTitle:{
        fontSize: 12,
        fontWeight: 'bold',
        color: '#747474',
        paddingBottom: 10
    },
    icon:{
        fontSize: 9,
        marginRight: 0
    },
    fontSmall:{
        fontSize: 9,
        marginLeft: -10,
    }
});