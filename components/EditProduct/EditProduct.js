import React, { Component } from 'react';
import { Platform, StyleSheet,ImageBackground, TextInput } from 'react-native';
import { Container, Header, Left, Right, Button, Title, Form, Item, Text, Icon, Row, Col, View, } from 'native-base';
import { withNavigation } from 'react-navigation';


class EditProduct extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true, 
            quantity:'',
            filterType:'',
            productNr:'',
            productTitle: '',
            productId: ''
        }
    }

    componentWillMount(){
        const { navigation } = this.props;

        let productNR = navigation.getParam('productNr', 'NO-NR');
        let productID = navigation.getParam('productId', 'NO-ID');
        let productTITLE = navigation.getParam('productTitle', 'NO-TITLE');
        let productQTY = navigation.getParam('productQty', 'Quantity');
        let productFILTER = navigation.getParam('productFilter', 'Filter Type');

        this.setState({
            productTitle: productTITLE,
            productNr: productNR,
            quantity: productQTY,
            filterType: productFILTER,
            productId: productID
        })
    }

    _editProductButton(){
        const apikey = 1;
        const token =  tokenx;
        const access = accessx;
        let filtertype = this.state.filterType;
        let quantity = this.state.quantity;
        let sku = this.state.productNr;
        let productId = this.state.productId;
        
        fetch('https://filterandindustrial.com.au/apix/Kits/EditProduct', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ apikey:apikey,
                apikey: apikey,
                token: token,
                access: access,
                quantity: quantity,
                filtertype: filtertype,
                sku: sku,
                kitproductID: kitproductIDxx,
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
             //check for errors
              if(responseJson['code'] != 0 ){
                  alert(responseJson['message']);
                  if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                      this.props.navigation.push('SignInScreen');
                      return false;
                  }
              }
            this.props.navigation.push('ViewKit');
        })
        .catch((error) => {
            console.error(error);
        })
    }

    render() {

        return (
            <ImageBackground source={require('./../../assets/img/bg.png')} style={styles.imgWrapper}>
                <Container style={styles.container}>
                    <Header transparent style={styles.header}>
                        <Left>
                            <Button transparent iconLeft style={{width: 200, height: 40}} onPress={() => { this.props.navigation.goBack() }}>
                                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                    <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                                    <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>EDIT PRODUCT</Title>
                                </View>
                            </Button>
                        </Left>
                        <Right style={{marginRight: 0}}>
                            <Button iconRight style={styles.buttonRight} onPress={() => { this._editProductButton() }}>
                                <Text style={styles.text}>UPDATE</Text>
                            </Button>
                        </Right>
                    </Header>
                <Container style={styles.containerContent}>
                    <View style={{paddingBottom: 20}}>
                        <Text style={{fontSize: 12, fontWeight: '100'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim leo, fermentum in lacinia nec, iaculis eget libero</Text>
                    </View>
                    <Form>
                        <View style={{paddingBottom: 50, flex: 1}}>
                            <Button block 
                                style={{height: 30, backgroundColor: '#fff', borderRadius: 5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8,shadowRadius: 2, elevation: 5}} 
                                onPress={() => { this.props.navigation.navigate('SearchEditProduct') }}
                            >
                                <Row>
                                    <Col style={{justifyContent: 'center'}}>
                                        <Text style={{textAlign: 'left', color: '#000'}}>{this.state.productTitle}</Text>
                                    </Col>
                                    <View style={{alignItems:'flex-end', justifyContent: 'center', paddingRight: 15}}>
                                        <Icon type="FontAwesome" name='chevron-right' style={{color: '#FF0000', fontSize: 16 }}/>
                                    </View>
                                </Row>
                            </Button>
                        </View>
                        <Form>
                            <View style={{paddingBottom: 20}}>
                                <Item rounded style={styles.inputContainer}>
                                    <TextInput 
                                        placeholderTextColor="#C1C1C1"
                                        keyboardType='numeric'
                                        style={styles.input2} 
                                        placeholder={this.state.quantity} 
                                        onChangeText={(quantity) => this.setState({quantity})} 
                                        value={this.state.quantity} 
                                    />
                                </Item>
                            </View>
                            <View style={{paddingBottom: 20}}>
                                <Item rounded style={styles.inputContainer}>
                                    <TextInput placeholderTextColor="#C1C1C1" 
                                        style={styles.input2} placeholder={this.state.filterType} onChangeText={(filterType) => this.setState({filterType})} 
                                        value={this.state.filterType} 
                                    />
                                </Item>
                            </View>
                        </Form>
                    </Form>
                </Container>
                </Container>
            </ImageBackground>
        );
    }
}

export default withNavigation(EditProduct);

const styles = StyleSheet.create({
    imgWrapper: {
        flex: 1
    },
    container:{
        backgroundColor: 'transparent'
    },
    header:{
        paddingTop: 0,
        height: 50,
        alignItems: 'center',
    },
    button:{
        backgroundColor: '#FF0000',
        borderRadius: 10,
        height: 30,
    },
    iconLeft:{
        color: '#FF0000',
        fontSize: 16, 
        marginLeft: 0,
        ...Platform.select({
            android:{
                marginLeft: -7,
            }
        })
    },
    buttonRight:{
        backgroundColor: '#FF0000',
        borderRadius: 10,
        height: 30,
    },
    text:{
        fontWeight: 'bold'
    },
    icon:{
        fontSize: 12,
        color: '#fff'
    },
    containerContent:{
        backgroundColor: 'transparent',
        //borderColor: 'transparent',
        paddingHorizontal: 10
    },
    inputContainer:{
        borderRadius: 5,
        height: 30,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,  
        elevation: 5,
    },
    input:{
        fontSize: 12,
        fontWeight: 'bold',
        color: '#747474'
    },
    input2:{
        fontSize: 12,
        //fontWeight: 'bold',
        color: '#747474',
        paddingHorizontal: 10,
        flex: 1,
    },

    textAreaContainer:{
        paddingTop: 30
    },
    textArea:{
        borderRadius: 10,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,  
        elevation: 5,
        padding: 5,
        fontSize: 12,
        color: '#747474'
    }
});