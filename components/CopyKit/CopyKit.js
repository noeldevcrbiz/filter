import React, { Component } from 'react';
import {Platform, StyleSheet,TextInput} from 'react-native';
import {Header, Left, Right, Button, Icon, Title, Text, Container, Item, Input, View, Form} from 'native-base';
import { withNavigation } from 'react-navigation';

class CopyKit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            kitName: '',
            kitDescription: '' 
        }
    }

    handleClick = () => {
        const kitTitle = this.state.kitName;
        const kitDescription = this.state.kitDescription;

        if(!kitTitle){
            alert('Please add kit title');
            return false;
        }
        if(!kitDescription){
            alert('Please add kit description');
            return false;
        }

        return fetch('https://filterandindustrial.com.au/apix/Kits/copy', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: 1,
                token: tokenx,
                access: accessx,
                kitid: kitIDx,
                name: kitTitle,
                short_description: kitDescription
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            //check for errors
            if(responseJson['code'] != 0 ){
                alert(responseJson['message']);
                if(responseJson['code'] == 1 || responseJson['code'] == 4 || responseJson['code'] == 8){
                    this.props.navigation.push('SignInScreen');
                    return false;
                }
            }
            this.props.navigation.push('Dashboard')
        })
        .catch((error) => {
            console.error(error);
        });
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header transparent style={styles.header}>
                    <Left>
                        <Button transparent iconLeft style={{width: 200, height: 40}} onPress={() => { this.props.navigation.navigate('ViewKit') }}>
                            <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                <Icon type="FontAwesome" name='chevron-left' style={styles.iconLeft}/>
                                <Title style={{color: '#000', paddingLeft: 10, fontSize: 16}}>COPY KIT</Title>
                            </View>
                        </Button>
                    </Left>
                    <Right>
                        <Button iconRight style={styles.button} onPress={this._addKitAsync} >
                            <Text style={styles.text}>SAVE</Text>
                            <Icon type='FontAwesome' name='chevron-right' style={styles.icon}/>
                        </Button>
                    </Right>
                </Header>
                <View style={{paddingHorizontal: 10, paddingTop: 20}}>
                    <View style={{paddingBottom: 20}}>
                        <Text style={{fontSize: 12}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim leo, fermentum in lacinia nec, iaculis eget libero</Text>
                    </View>
                    <Form>
                        <Item rounded style={styles.inputContainer}>
                            <TextInput placeholderTextColor="#C1C1C1" 
                                placeholder="Kit Name"
                                style={styles.input} onChangeText={(kitName) => this.setState({kitName})} 
                                value={this.state.kitName} 
                            />
                        </Item>
                        <View style={styles.textAreaContainer}>
                            <TextInput bordered placeholderTextColor="#C1C1C1"
                                placeholder="Kit Description" 
                                style={styles.textArea} onChangeText={(kitDescription) => this.setState({kitDescription})} 
                                value={this.state.kitDescription} 
                                multiline={true} 
                                numberOfLines={10}   
                            />
                        </View>
                    </Form>
                </View>
            </Container>
        );
    }

    _addKitAsync = async () => {
        responsex = this.handleClick();
    };
}

export default withNavigation(CopyKit);


const styles = StyleSheet.create({
    header:{
        height: 40  
    },
    button:{
      backgroundColor: '#FF0000',
      borderRadius: 10,
      height: 30,
    },
    iconLeft:{
      color: '#FF0000', 
      fontSize: 16, 
      marginLeft: 0,
      ...Platform.select({
        android:{
          marginLeft: -10,
        }
      })
    },
    text:{
      fontWeight: 'bold'
    },
    icon:{
      fontSize: 12,
      color: '#fff'
    },
     container:{
      backgroundColor: 'transparent',
      borderColor: 'transparent',
  },
  inputContainer:{
    borderRadius: 10,
    height: 30,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5
  },
  textAreaContainer:{
    paddingTop: 30
  },
  input:{
    fontSize: 12,
    fontWeight: 'bold',
    color: '#000',
    paddingHorizontal: 15,
    flex: 1,
    flexDirection: 'row'
  },
  textArea:{
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5,
    padding: 5,
    fontSize: 12,
    color: '#000',
    fontWeight: 'bold',
    height: 150,
    textAlignVertical: 'top'
  },
});