/**
*Splash
*
*
*/
import React, {Component} from 'react';
import {StyleSheet, Text, View, StatusBar, ImageBackground } from 'react-native';

export default class Splash extends Component {
	render(){
		return(
			<ImageBackground source={require('./assets/img/bg.png')} style={styles.wrapper}>
				<View style={styles.wrapperTitle}>
					<Text style={styles.textTitle}>Filter and Industrial</Text>
				</View>
				<View>
					<Text style={styles.subTitle}>coolrunningbusinesssolution.com.au</Text>
				</View>
			</ImageBackground>
		)
	}
}

const styles = StyleSheet.create({
	wrapper:{
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	wrapperTitle:{
		flex: 1,
		justifyContent: 'center'
	},
	textTitle:{
		fontSize: 35,
		fontWeight: 'bold'
	},
	subTitle:{
		paddingBottom: 10
	}

});